//
//  MenuController.h
//  Ekranda
//
//  Created by Mesut Dağ on 13/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView* menuTable;

//@property (nonatomic, strong) IBOutlet UILabel* connectionText;
//@property (nonatomic, strong) IBOutlet UIImageView* connectionImage;
//@property (nonatomic, strong) IBOutlet UIButton* connectionButton;

-(void)loadUI:(NSMutableArray*) items;
-(void)updateFavCount;
-(void)stopDetectorWithImages;
-(void)startDetectorWithImages;
-(IBAction)connectionClicked:(id)sender;

@end
