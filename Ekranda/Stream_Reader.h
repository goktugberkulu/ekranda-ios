/**
 * Copyright (c) 2014 Civolution B.V. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Civolution B.V. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Civolution B.V.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : ftristani
 *
 * Stream_Reader.h
 */

#include "AwmSyncDetector.h"

@class SyncNowDetector;

@interface Stream_Reader : NSObject <AwmSyncDetectorDelegate>
{
@private
    SyncNowDetector           *mUI;
    AwmSyncDetector                 *mDetector;
    NSFileHandle                    *mFileHandler;
    int                             mPayloadID;
    int                             mTimestamp;
    BOOL                            mTimestampLoop;
    BOOL                            mLog;
    NSString                        *mLicense; 
@public
    BOOL                            mIsRunning;
    BOOL                            mIsRecording;
}

@property (assign, nonatomic) BOOL	mIsRunning;
@property (assign, nonatomic) BOOL	mIsRecording;

- (id) initWithUI:(SyncNowDetector *) VC license:(NSString *) license payloadID:(int) payloadID timestamp:(int) timestamp timestampLoop:(BOOL) timestampLoop log:(BOOL) log;
- (int) readStream:(void *) buffer;
- (void) stopReader;
- (void) startReader:(NSNumber *) record;
- (NSString *) getVersion;
- (BOOL) getWmkPresence;

@end