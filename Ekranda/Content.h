//
//  Content.h
//  Ekranda
//
//  Created by Mesut Dağ on 14/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Content : NSObject

@property (nonatomic) int contentId;
@property (nonatomic) int categoryId;
@property (nonatomic, strong) NSString* contentImgUrl;
@property (nonatomic, strong) NSString* contentTitle;
@property (nonatomic) int contentLikeCount;
//@property (nonatomic, strong) NSString* contentChannels;
@property (nonatomic) double contentStartDateTime;
@property (nonatomic) double contentEndDateTime;
@property (nonatomic, strong) NSMutableArray* items;
@property (nonatomic) BOOL isNew;

@property (nonatomic) BOOL contentIsHTML5;
@property (nonatomic, strong) NSString* contentHTML5Type;
@property (nonatomic, strong) NSString* contentHTML5Url;

-(void)setObjectStatus;

@end
