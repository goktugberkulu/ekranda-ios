//
//  SyncClock.h
//  Ekranda
//
//  Created by Mesut Dağ on 03/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface SyncClock : NSObject

@property (nonatomic) AVAudioPlayer* player;

-(void)start;
-(void)stop;
-(long long)getTime;
-(void)setTime:(long long) timeToSet;

@end
