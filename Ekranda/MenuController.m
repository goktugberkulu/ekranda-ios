//
//  MenuController.m
//  Ekranda
//
//  Created by Mesut Dağ on 13/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "MenuController.h"
#import "MenuLine.h"
#import "MenuCategory.h"
#import "HomeController.h"
#import "EkrandaAppDelegate.h"
#import "FavoriteViewController.h"
#import "InfoController.h"

NSMutableArray* items;

@implementation MenuController


@synthesize menuTable;

-(void)loadUI:(NSMutableArray*) categories{
    NSLog(@"load Menu");
    
    self.menuTable.dataSource = self;
    self.menuTable.delegate = self;
    items = categories;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.menuTable reloadData];
    });
    self.menuTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self updateFavCount];
}

-(void)updateFavCount {
//    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [self.favCount setText:[NSString stringWithFormat:@"%li ürün",(long)app.favorites.count]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"load Menu");
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return items.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuLine* cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MenuLine" bundle:nil] forCellReuseIdentifier:@"menuCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    }
    
    MenuCategory* category = [items objectAtIndex:indexPath.row];
    [cell.titleLabel setText:category.categoryTitle];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MenuCategory* category = [items objectAtIndex:indexPath.row];
    if([category.categoryType isEqualToString:@"home"])
        category = nil;
   
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showHome:category withToggle:YES];
}


-(IBAction)favoritesClicked:(id)sender{
    FavoriteViewController* fvc = [[FavoriteViewController alloc] init];
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    fvc.favorites = app.favorites;
    UINavigationController* centerNavigation = (UINavigationController *)app.drawerController.centerViewController;
    
    [centerNavigation pushViewController:fvc animated:YES];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(IBAction)infoClicked:(id)sender{
    
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    AboutViewController* fvc = [storyboard instantiateViewControllerWithIdentifier:@"About"];
//    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
//    UINavigationController* centerNavigation = (UINavigationController *)app.drawerController.centerViewController;
//    
//    [centerNavigation pushViewController:fvc animated:YES];
//    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    InfoController* fvc = [[InfoController alloc] init];
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController* centerNavigation = (UINavigationController *)app.drawerController.centerViewController;
    
    [centerNavigation pushViewController:fvc animated:YES];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(IBAction)connectionClicked:(id)sender {
    if([sender tag] == 1){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"micOpen"];
        [self stopDetectorWithImages];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"micOpen"];
        [self startDetectorWithImages];
    }
}

-(void)stopDetectorWithImages{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app stopDetector];
//    [self.connectionButton setTag:0];
//    [self.connectionImage setImage:[UIImage imageNamed:@"connect_off.png"]];
//    [self.connectionText setText:@"Kapalı"];
//    [self.connectionText setTextColor:[UIColor colorWithRed:242.0/255.0 green:91.0/255.0 blue:90.0/255.0 alpha:1]];
    
//    UINavigationController* centerNavigation = (UINavigationController *)app.drawerController.centerViewController;
//    HomeController* home = [centerNavigation.viewControllers lastObject];
//    [home warnSignalization:NO];
}

-(void)startDetectorWithImages{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app startDetector];
//    [self.connectionButton setTag:1];
//    [self.connectionImage setImage:[UIImage imageNamed:@"connect_on.png"]];
//    [self.connectionText setText:@"Açık"];
//    [self.connectionText setTextColor:[UIColor colorWithRed:174.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1]];
    
//    UINavigationController* centerNavigation = (UINavigationController *)app.drawerController.centerViewController;
//    HomeController* home = [centerNavigation.viewControllers lastObject];
//    [home warnSignalization:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
