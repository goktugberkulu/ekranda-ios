//
//  Content.m
//  Ekranda
//
//  Created by Mesut Dağ on 14/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "Content.h"
#import "EkrandaAppDelegate.h"

@implementation Content
@synthesize contentStartDateTime,isNew;

-(void)setObjectStatus{
    NSDate *first = [NSDate dateWithTimeIntervalSince1970:contentStartDateTime];
    NSDate *now = [NSDate networkDate];
    
    NSTimeInterval diff = [now timeIntervalSinceDate:first];
    long long difMinutes = floor(diff/60);
    
    if(difMinutes<15)
        isNew = YES;
    else
        isNew = NO;
}

@end
