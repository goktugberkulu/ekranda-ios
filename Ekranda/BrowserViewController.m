//
//  BrowserViewController.m
//  Ekranda
//
//  Created by Mesut Dağ on 02/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "BrowserViewController.h"
#import "Core.h"
#import "JsonParser.h"
#import "EkrandaAppDelegate.h"

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end

@implementation BrowserViewController



@synthesize eWebView;

- (void)viewDidLoad {
    [super viewDidLoad];

    [Core addLoading:self.view withDim:NO dimString:nil];
    
    eWebView.delegate = self;
    eWebView.scalesPageToFit = YES;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)loadUrl{
 
    __block NSString *sepetKey = @"Sepet";
    __block NSString* url = [[NSString stringWithFormat:@"%@",s_LIDYANA_OPEN_CART] stringByReplacingOccurrencesOfString:@"@@GID@@" withString:[Core getDeviceId]];
    
    EkrandaAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
  
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      
        if(delegate.hbSecureObject != nil && self.goType != s_GO_LIDYANA){
            
            BOOL result = [JsonParser checkHBCard:delegate.hbSecureObject];
            if(result){
                url = [delegate.hbSecureObject objectForKey:@"redirectUrl"];
                sepetKey = @"Sepet-Hepsiburada";
            }else{
                [delegate saveHBObject:nil];
            }
        }
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [eWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        });
        
        if(!self.isWebPage){
            [delegate ganSendView:sepetKey];
            //[TapTarget ttlog:sepetKey withValue:@"" andScore:3];
        }
    });
    
   
    
    NSLog(@"go url %@",url);
}

-(void)loadProductUrl:(NSString *)url{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [eWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    });
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [Core removeLoading:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
