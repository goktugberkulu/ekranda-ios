/**
 * Copyright (c) 2014 Civolution B.V. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Civolution B.V. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Civolution B.V.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : ftristani
 *
 * Recorder.h
 */

#import <AudioToolbox/AudioToolbox.h>
#import <Foundation/Foundation.h>
#include "CAStreamBasicDescription.h"
#include "CAXException.h"
#define kNumberRecordBuffers	3


@class Stream_Reader;

class Recorder
{
public:
	Recorder();
	~Recorder();
	void            StartRecord();
	void            StopRecord();
	Boolean         IsRunning() const			{ return mIsRunning; }
	void            InitRecorder(Stream_Reader *newStreamReader,Float64 fs, int bitsPerChannel, int bufferSize, int numChannel);
    Stream_Reader   *stream_reader;
    Float64         mFs;
	int             mBitsPerChannel;
	int             mBufferSize;
    float           mBufferLength;
	int             mNumChannel;
    AudioQueueRef				mQueue;
	AudioQueueBufferRef			mBuffers[kNumberRecordBuffers];
    static void MyInputBufferHandler(void *									inUserData,
									 AudioQueueRef							inAQ,
									 AudioQueueBufferRef					inBuffer,
									 const AudioTimeStamp *					inStartTime,
									 UInt32									inNumPackets,
									 const AudioStreamPacketDescription*	inPacketDesc);
    
private:
	Boolean						mIsRunning;
    CAStreamBasicDescription	mRecordFormat;
    void SetupAudio();
};