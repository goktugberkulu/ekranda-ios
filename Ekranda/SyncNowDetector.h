//
//  SyncNowDetector.h
//  Ekranda
//
//  Created by Mesut Dağ on 27/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <AVFoundation/AVAudioSession.h>
#import "Stream_Reader.h"

@interface SyncNowDetector : NSObject

@property (nonatomic, strong) NSString *mTraceText;

-(void)start;
-(void)stop;

- (IBAction)streamReader:(id)sender;
- (IBAction)record:(id)sender;
- (IBAction)getWmkPresence:(id)sender;
- (void) Trace:(NSString *) textToTrace;
- (void) onPayload:(NSNumber *)timeStamp;

@end
