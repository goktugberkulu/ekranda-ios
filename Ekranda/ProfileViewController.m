//
//  ProfileViewController.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//


#import "ProfileViewController.h"
#import "UIColor+Ekranda.h"
#import "ProfileTableViewCell.h"
#import "EkrandaAppDelegate.h"

@interface ProfileViewController ()
@property (strong, nonatomic) NSArray *labelArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *pointsButton;
@property (weak, nonatomic) IBOutlet UIButton *logOutButton;
@property (strong, nonatomic) NSArray *fieldArray;
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labelArray = @[@"Adı", @"Soyadı", @"E-posta Adresi", @"Şifre", @"Şifre (Tekrar)"];
    self.fieldArray = @[@"John", @"Doe", @"johndoe@gmail.com", @"******", @"******"];
    self.title = @"Profilim";
    
    [self.navigationController.navigationBar setTintColor:[UIColor eiWhiteColor]];
    self.scrollView.contentSize = self.view.frame.size;
    UIBarButtonItem *yourButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Kaydet"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(save)];
    self.navigationItem.rightBarButtonItem = yourButton;
    self.navigationItem.leftBarButtonItem.title = @" ";
    self.pointsButton.titleLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular];
    self.logOutButton.titleLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular];

 //   self.tableView.rowHeight = UITableViewAutomaticDimension;
 //   [self.tableView setEstimatedRowHeight:35.0];
    // Do any additional setup after loading the view.
}

- (void) save {
    
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self presentViewController:app.drawerController animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
    [self.tableView sizeToFit];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
    cell.label.text = [self.labelArray objectAtIndex:indexPath.row];
    cell.field.text = [self.fieldArray objectAtIndex:indexPath.row];
    [self.tableView sizeToFit];

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.tableView.frame.size.height / 5;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
