//
//  CustomViewController.m
//  Ekranda
//
//  Created by Mesut Dağ on 23/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "CustomViewController.h"
#import "EkrandaAppDelegate.h"
#import "HomeController.h"
#import "BrowserViewController.h"
#import "Core.h"
#import "SearchViewController.h"
#import "UIColor+Ekranda.h"


@interface CustomViewController ()

@end

@implementation CustomViewController

@synthesize lblSignalization;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createStaticButtons];
    
    if([self isKindOfClass:[HomeController class]])
        [self addMenuButton];
    else if([self isKindOfClass:[SearchViewController class]])
        self.navigationItem.leftBarButtonItem = nil;
    else
        [self addBackButton];
    
    if(![self isKindOfClass:[BrowserViewController class]])
       [self addBasketButton];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if([self isKindOfClass:[HomeController class]])
         [app.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    else
        [app.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
}

-(void)switchMenu{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)addMenuButton{
    UIImage *myImage = [UIImage imageNamed:@"icnMenu.png"];
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton setImage:myImage forState:UIControlStateNormal];
    myButton.showsTouchWhenHighlighted = YES;
    myButton.frame = CGRectMake(0.0, 0.0, 30, 30);
    
    [myButton addTarget:self action:@selector(switchMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc]
                             initWithCustomView:myButton];
    self.navigationItem.leftBarButtonItem = left;
}

-(void)addBackButton{
    UIImage *myImage = [UIImage imageNamed:@"icnBack.png"];
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton setImage:myImage forState:UIControlStateNormal];
    myButton.showsTouchWhenHighlighted = YES;
    myButton.frame = CGRectMake(0.0, 0.0, 30, 30);
    [myButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc]
                             initWithCustomView:myButton];
    self.navigationItem.leftBarButtonItem = left;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    [self.navigationController.interactivePopGestureRecognizer setEnabled:YES];
}

-(void) popBack {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createStaticButtons{
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor eiFadedRedColor]];
    //UIImage *navBarBg = [[UIImage imageNamed:@"topbar_bg@2X.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    
  //  [self.navigationController.navigationBar setBackgroundImage:navBarBg forBarMetrics:UIBarMetricsDefault];
  
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIImage* center;
    int width;
    int height;
    if ([[UIScreen mainScreen] bounds].size.height > 480) {
        center = [UIImage imageNamed:@"imgHeaderLogo.png"];
        width = center.size.width;
        height = center.size.height;
        
    } else {
        center = [UIImage imageNamed:@"imgHeaderLogo.png"];
        width = 114;
        height = 24;
    }
    
//    UIButton *centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [centerButton setImage:center forState:UIControlStateNormal];
//    [centerButton setContentMode:UIViewContentModeScaleAspectFit];
//    centerButton.frame = CGRectMake(0.0, 0.0, width, height);
//    [centerButton addTarget:self action:@selector(logoClicked:) forControlEvents:UIControlEventTouchUpInside];
    CGRect rect = CGRectMake(0.0, 0.0, width, height);
    UIView* centerButton = [[UIView alloc] initWithFrame:rect];
    UITapGestureRecognizer* organizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchLogo:)];
    [centerButton addGestureRecognizer:organizer];
    
    UIImageView* centerImage = [[UIImageView alloc] initWithFrame:rect];
    [centerImage setImage:center];
    [centerButton addSubview:centerImage];
    
    lblSignalization = [[UITextView alloc] initWithFrame:CGRectMake(width - 80, 10, 80, 24)];
    [lblSignalization setText:@"mic ready"];
    [lblSignalization setTextAlignment:NSTextAlignmentCenter];
    [lblSignalization setTextColor:[UIColor greenColor]];
    [lblSignalization setBackgroundColor:[UIColor clearColor]];
    [lblSignalization setHidden:YES];
    [centerButton addSubview:lblSignalization];
    
    self.navigationItem.titleView = centerButton;
    
}

NSTimer* warnTimer;

-(void)warnSignalization:(BOOL)isOpen {
    if (isOpen) {
        warnTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(setVisibilityLblSignal) userInfo:nil repeats:YES];
    } else {
        [warnTimer invalidate];
        warnTimer = nil;
        lblSignalization.tag = 1;
        [lblSignalization setHidden:YES];
    }
}

-(void) setVisibilityLblSignal {
    if (lblSignalization.tag == 0) {
        lblSignalization.tag = 1;
        [lblSignalization setHidden:YES];
    } else {
        lblSignalization.tag = 0;
        [lblSignalization setHidden:NO];
    }
}

-(void)addBasketButton{

    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIImage *right1 = [UIImage imageNamed:@"icnSearch.png"];
    UIButton *rightb1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightb1 setImage:right1 forState:UIControlStateNormal];
    rightb1.showsTouchWhenHighlighted = YES;
    rightb1.frame = CGRectMake(0.0, 0.0, 30, 30);
    [rightb1 addTarget:self action:@selector(searchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *right1item = [[UIBarButtonItem alloc]
                                   initWithCustomView:rightb1];

    
    UIImage *right2 = [UIImage imageNamed:@"icnSepet.png"];
    UIButton *rightb2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightb2 setImage:right2 forState:UIControlStateNormal];
    rightb2.showsTouchWhenHighlighted = YES;
    rightb2.frame = CGRectMake(0.0, 0.0, 30, 30);
    [rightb2 addTarget:self action:@selector(basketClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *right2item = [[UIBarButtonItem alloc]
                                   initWithCustomView:rightb2];
    
    [buttons addObject:right2item];
    [buttons addObject:right1item];
    self.navigationItem.rightBarButtonItems = buttons;
}

//-(IBAction)connectClicked:(id)sender{
//    UIButton* senderBtn = (UIButton *)sender;
//    EkrandaAppDelegate* app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
//    if(senderBtn.tag == 10){
//        [senderBtn setImage:[UIImage imageNamed:@"connect_off_ico@2X.png"] forState:UIControlStateNormal];
//        senderBtn.tag = 11;
//        [app stopDetector];
//        [Core addToast:self.view withString:@"Dinleme modu kapatıldı"];
//        
//    }else{
//        [senderBtn setImage:[UIImage imageNamed:@"connect_on_ico@2X.png"] forState:UIControlStateNormal];
//        senderBtn.tag = 10;
//        [app startDetector];
//        [Core addToast:self.view withString:@"Dinleme modu açıldı"];
//    }
//}

-(IBAction)basketClicked:(id)sender{
    BrowserViewController* bc = [[BrowserViewController alloc] init];
    [bc loadUrl];
    [self.navigationController pushViewController:bc animated:YES];

}

-(IBAction)searchClicked:(id)sender{

    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Search" bundle:[NSBundle mainBundle]];
    UIViewController *searchViewController = [storyboard instantiateInitialViewController];
    searchViewController.navigationItem.leftBarButtonItem = nil;
    [self presentViewController:searchViewController animated:YES completion:nil];
    
}

-(IBAction)logoClicked:(id)sender{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showHome:0 withToggle:NO];
}

-(void)touchLogo:(UIGestureRecognizer*)organizer {
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app showHome:0 withToggle:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
