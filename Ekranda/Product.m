//
//  Product.m
//  Ekranda
//
//  Created by Mesut Dağ on 24/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize itemBrand,itemId,itemImage,itemIsSale,itemName,itemPrice,itemProductId,itemStoreLink,contentId;

- (id)initWithCoder: (NSCoder *)coder
{
    if (self = [super init])
    {
        self.itemBrand = [coder decodeObjectForKey:@"itemBrand"];
        self.itemId = [coder decodeIntForKey:@"itemId"];
        self.itemImage = [coder decodeObjectForKey:@"itemImage"];
        self.itemIsSale = [coder decodeBoolForKey:@"itemIsSale"];
        self.itemName = [coder decodeObjectForKey:@"itemName"];
        self.itemPrice = [coder decodeObjectForKey:@"itemPrice"];
        self.itemProductId = [coder decodeObjectForKey:@"itemProductId"];
        self.itemStoreLink = [coder decodeObjectForKey:@"itemStoreLink"];
        self.contentId = [coder decodeIntForKey:@"contentId"];
        self.contentTitle = [coder decodeObjectForKey:@"contentTitle"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder
{
    [coder encodeObject:self.itemBrand forKey:@"itemBrand"];
    [coder encodeInt:self.itemId forKey:@"itemId"];
    [coder encodeObject:self.itemImage forKey:@"itemImage"];
    [coder encodeBool:self.itemIsSale forKey:@"itemIsSale"];
    [coder encodeObject:self.itemName forKey:@"itemName"];
    [coder encodeObject:self.itemPrice forKey:@"itemPrice"];
    [coder encodeObject:self.itemProductId forKey:@"itemProductId"];
    [coder encodeObject:self.itemStoreLink forKey:@"itemStoreLink"];
    [coder encodeInt:self.contentId forKey:@"contentId"];
    [coder encodeObject:self.contentTitle forKey:@"contentTitle"];
}

@end
