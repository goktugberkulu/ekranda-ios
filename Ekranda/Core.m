//
//  Core.m
//  Ekranda
//
//  Created by Mesut Dağ on 30/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "Core.h"

@implementation Core

static UIView* notifView;

+(void)addLoading:(UIView*)mainView withDim:(BOOL)dim dimString:(NSString*)dimText{
    
    int x = [[UIScreen mainScreen] bounds].size.width / 2 - 50;
    int y = [[UIScreen mainScreen] bounds].size.height / 2 - 80;
    
    UIView* loadingView = [[UIView alloc] initWithFrame:CGRectMake(x, y, 100, 80)];
    
    UIImage* backImage = [UIImage imageNamed:@"app_ico_back.png"];
    UIImageView *backImageView = [[UIImageView alloc] initWithImage:backImage];
    [backImageView setFrame:CGRectMake(0, 0, 100, 80)];
    [backImageView setContentMode:UIViewContentModeScaleAspectFit];
    
    UIImage* topImage = [UIImage imageNamed:@"app_ico_top.png"];
    UIImageView *topImageView = [[UIImageView alloc] initWithImage:topImage];
    [topImageView setContentMode:UIViewContentModeScaleAspectFit];
    [topImageView setFrame:CGRectMake(0, 0, 100, 80)];
    
    
    [loadingView addSubview:backImageView];
    [loadingView addSubview:topImageView];
    
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 0.85f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [backImageView.layer removeAllAnimations];
    [backImageView.layer addAnimation:rotation forKey:@"Spin"];
    
    CABasicAnimation* zoomOut = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    zoomOut.duration = 1;
    zoomOut.toValue = [NSNumber numberWithFloat:0.9];
    zoomOut.duration = 0.9f; // Speed
    zoomOut.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    zoomOut.autoreverses = YES;
    [topImageView.layer removeAllAnimations];
    [topImageView.layer addAnimation:zoomOut forKey:@"Spin"];
    loadingView.tag = 1001;
    [mainView addSubview:loadingView];
}

+(void)removeLoading:(UIView*)mainView{
    [[mainView viewWithTag:1001] removeFromSuperview];
    [[mainView viewWithTag:1002] removeFromSuperview];
}

+(void)addToast:(UIView*)mainView withString:(NSString *)toastText{
    
    [[mainView viewWithTag:10009] removeFromSuperview];
    
    UIView* toastView = [[UIView alloc] initWithFrame:CGRectMake(20, mainView.frame.size.height+20, 280, 30)];
    [toastView setBackgroundColor:[UIColor colorWithRed:242.0/255.0 green:91.0/255.0 blue:90.0/255.0 alpha:1]];
    [toastView setAlpha:0];
    UILabel* toastLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 30)];
    [toastLabel setTextColor:[UIColor whiteColor]];
    [toastLabel setFont:[toastLabel.font fontWithSize:15]];
    toastLabel.textAlignment = NSTextAlignmentCenter;
    
    [toastLabel setText:toastText];
    [toastView addSubview:toastLabel];
    toastView.tag = 10009;
    
    [toastView.layer setShadowColor:[UIColor blackColor].CGColor];
    [toastView.layer setShadowOpacity:0.6];
    [toastView.layer setShadowRadius:2.0];
    [toastView.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    [toastView.layer setCornerRadius:4.0f];
    
    [mainView addSubview:toastView];
    
    
    [UIView animateWithDuration:0.7
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         toastView.alpha = 0.95;
                         [toastView setFrame:CGRectMake(20, mainView.frame.size.height-40, 280, 30)];

                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.7
                                               delay:2
                                             options: UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              toastView.alpha = 0;
                                              [toastView setFrame:CGRectMake(20, mainView.frame.size.height+20, 280, 30)];
                                              
                                          }
                                          completion:^(BOOL finished) {
                                              [toastView removeFromSuperview];
                                          }];
    }];
}

+(NSString *)getDeviceId{
    
    NSString* userId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    if([userId length] < 40)
    {
        int diff = 40 - (int)[userId length];
        for(int i=0;i<diff;i++)
            userId = [userId stringByAppendingString:@"0"];
    }
    return userId;
}

@end
