//
//  SimilarProductsCollectionViewController.m
//  EkrandaUI
//
//  Created by Goktug on 15/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "SimilarProductsCollectionViewController.h"
#import "SimilarProductsCollectionViewCell.h"

@interface SimilarProductsCollectionViewController ()

@end

@implementation SimilarProductsCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 130 * 8, 240) collectionViewLayout:layout];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"SimilarProductsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"similarCell"];
    [self.view addSubview:self.collectionView];
    

    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
//    
//    // Register cell classes
 //   [self.collectionView registerClass:[SimilarProductsCollectionViewCell class] forCellWithReuseIdentifier:@"similarCell"];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 8;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SimilarProductsCollectionViewCell *cell = (SimilarProductsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"similarCell" forIndexPath:indexPath];
    if (cell == nil) {
        
        [collectionView dequeueReusableCellWithReuseIdentifier:@"similarCell" forIndexPath:indexPath];
        
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(124, 230);
}


@end
