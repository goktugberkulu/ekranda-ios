//
//  UIFont+Ekranda.h
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Ekranda)

+ (UIFont *)eiHeaderTitleSFFont;
+ (UIFont *)eiNavigationSearchPlaceholderFont;
+ (UIFont *)eiCellLabelFont;
+ (UIFont *)eiCellTitleFont;

@end
