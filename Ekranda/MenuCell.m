//
//  MenuCell.m
//  Ekranda
//
//  Created by Mesut Dağ on 16/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
