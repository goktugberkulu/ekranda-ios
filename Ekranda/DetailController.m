//
//  DetailController.m
//  Ekranda
//
//  Created by Mesut Dağ on 23/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "DetailController.h"
#import "JsonParser.h"
#import "UIScrollView+APParallaxHeader.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "Product.h"
#import "UIColor+Ekranda.h"
#import "Core.h"
#import "EkrandaAppDelegate.h"
#import "ThirdPartyProduct.h"
#import "ProductSize.h"
#import "BrowserViewController.h"
#import "ObjectMapper.h"
#import "UIHeader.h"
#import "ImageWithZoom.h"
#import "ProductCommentsViewController.h"
#import "SimilarProductsCollectionViewController.h"


Content* contentDetail;
NSString* tapPath;

@interface DetailController ()
@property (nonatomic, strong, readonly) ImageWithZoom *zoomedImageView;
@property (nonatomic, strong) NSIndexPath *zoomedIndexPath;
@property (nonatomic, strong) ProductCommentsViewController *commentsVC;
@property (nonatomic, strong) SimilarProductsCollectionViewController *similarProductsVC;
@end

@implementation DetailController

@synthesize itemScroll,sizes,pickerContainer,pickerSize,content,contentDetailId;

UIView* loadImage;
SaleItemView* currentSIV;
EkrandaAppDelegate* app;

-(void)viewDidLoad {
    [super viewDidLoad];
    [Core addLoading:self.view withDim:NO dimString:nil];
    app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.itemScroll setHidden:YES];
    [self performSelectorInBackground:@selector(getDetails) withObject:nil];
    NSLog(@"header %f", self.headerView.frame.origin.y);
    NSLog(@"detail %f", self.detailLabel.frame.origin.y);
    NSLog(@"table %f", self.tableView.frame.origin.y);
}

-(void)loadAds {
  
    self.adManager = [[SLTableViewAdManager alloc] initWithTableView:self.tableView
                                                          dataSource:contentDetail.items
                                                      viewController:self];
    [self.adManager
     getAdsWithEntity:@"dizi-detay-item"
     category:SLCategoryFashion
     successBlock:^{
         NSLog(@"ads loaded");
     }
     failureBlock:^(NSError *error) {
         NSLog(@"%@",error);
     }];
    
    self.adView = [[SLAdView alloc] initWithEntity:@"dizi-detay"
                                          category:SLCategoryFashion
                                            adSize:SLAdSizeMMA
                                rootViewController:self];
    
    self.adView.delegate = self;
    CGRect frame = self.adView.frame;
    frame.origin.y = self.view.bounds.size.height - SLAdSizeMMA.height;
    self.adView.frame = frame;
    [self.adView loadAd];
    
    
    self.intAdManager = [[SLInterstitialAdManager alloc] initWithEntity:@"interstitial-detay"  category:SLCategoryFashion];
    self.intAdManager.delegate = self;
    [self.intAdManager loadAd];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(content){
        [app ganSendView:[NSString stringWithFormat:@"Detay - %@",content.contentTitle]];
       
        if(self.isFavorite)
            tapPath = [NSString stringWithFormat:@"/Favorilerim/%@",content.contentTitle];
        else
            tapPath = self.category ? [NSString stringWithFormat:@"/%@/%@",self.category.categoryTitle,content.contentTitle] : [NSString stringWithFormat:@"/Anasayfa/%@",content.contentTitle];
        
        //[TapTarget ttlog:tapPath withValue:@"" andScore:1];
    }
    
}

-(void)getDetails{
    contentDetail = [JsonParser getContentDetails:content ? content.contentId : contentDetailId];
    [self performSelectorOnMainThread:@selector(fillTable) withObject:nil waitUntilDone:NO];
}


-(void)fillTable{
    [Core removeLoading:self.view];
    
    [self setupScrollView];
    [self setupHeaderView];
    [self setupDetailLabel];
    [self setupTableView];
    [self setupSegmentedView];
    self.itemScroll.contentSize = CGSizeMake(0, self.commentsView.frame.size.height + self.commentsView.frame.origin.y + 80);
    self.pickerSize.delegate = self;
    self.pickerSize.dataSource = self;

    if(contentDetailId) {
        [app ganSendView:[NSString stringWithFormat:@"Detay - %@",contentDetail.contentTitle]];
    }

    [self loadAds];
    
}

- (void)setupScrollView {
    self.itemScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self.itemScroll setHidden:NO];
    self.itemScroll.delegate = self;
    self.itemScroll.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.itemScroll];
}

- (void)setupHeaderView {
    UIHeader* mainView = [[[NSBundle mainBundle] loadNibNamed:@"UIHeader" owner:self options:nil]
                          objectAtIndex:0];
    [mainView setFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, 180)];
    [mainView.mainImage sd_setImageWithURL:[NSURL URLWithString:contentDetail.contentImgUrl]
                          placeholderImage:[UIImage imageNamed:@"stub.png"]];
    [mainView.likeLabel setText:[NSString stringWithFormat:@"%d", contentDetail.contentLikeCount]];
    mainView.detailLabel.text = contentDetail.contentTitle;
    mainView.contentId = contentDetail.contentId;
    self.headerView = mainView;
    [self.itemScroll addSubview:mainView];
}

- (void)setupDetailLabel {
    self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, self.headerView.frame.origin.y + self.headerView.frame.size.height + 35, self.view.frame.size.width, 50)];
    [self.detailLabel setText:contentDetail.contentTitle];
    self.detailLabel.numberOfLines = 0;
    [self.detailLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [self.detailLabel setTextColor:[UIColor colorWithRed:74.0 / 255.0 green: 74.0 / 255.0 blue: 74.0 / 255.0 alpha: 1.0]];
    [self.itemScroll addSubview:self.detailLabel];
}

- (void)setupTableView {
    self.tableView = [[UITableView alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView setFrame:CGRectMake(5, self.detailLabel.frame.size.height + self.detailLabel.frame.origin.y + 10, self.view.frame.size.width - 10, contentDetail.items.count * 215)];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductCell" bundle:nil]  forCellReuseIdentifier:@"detailCell"];
    [self.tableView reloadData];
    [self.itemScroll addSubview:self.tableView];
}

- (void)setupSegmentedView {
    
    NSArray *itemArray = [NSArray arrayWithObjects: @"Benzer Ürünler", @"Yorumlar", nil];
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    self.segmentedControl.frame = CGRectMake(0, self.tableView.frame.origin.y + self.tableView.frame.size.height + 15, self.view.frame.size.width, 50);
    [self setupSimilarProductsView];
    [self setupCommentsView];
    [self.segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents: UIControlEventValueChanged];
    self.segmentedControl.selectedSegmentIndex = 0;
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor eiFadedRedColor]} forState:UIControlStateSelected];
    

    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
    self.segmentedControl.subviews.lastObject.tintColor = [UIColor whiteColor];
    self.segmentedControl.subviews.firstObject.tintColor = [UIColor eiFadedRedColor];
    self.segmentedControl.subviews.firstObject.backgroundColor = [UIColor eiFadedRedColor];
    self.segmentedControl.subviews.firstObject.tintColor = [UIColor whiteColor];
    self.segmentedControl.subviews.lastObject.tintColor = [UIColor eiFadedRedColor];
    self.segmentedControl.subviews.lastObject.backgroundColor = [UIColor eiFadedRedColor];
    [self.itemScroll addSubview:self.segmentedControl];
    
}

- (void)segmentAction:(UISegmentedControl *)segment {
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.similarProductsView.hidden = NO;
            self.commentsView.hidden = YES;
            segment.subviews.lastObject.tintColor = [UIColor whiteColor];
            segment.subviews.firstObject.tintColor = [UIColor eiFadedRedColor];
            segment.subviews.firstObject.backgroundColor = [UIColor eiFadedRedColor];
            NSLog(@"at similar");
            break;
        case 1:
            self.similarProductsView.hidden = YES;
            self.commentsView.hidden = NO;
            
            segment.subviews.firstObject.tintColor = [UIColor whiteColor];
            segment.subviews.lastObject.tintColor = [UIColor eiFadedRedColor];
            segment.subviews.lastObject.backgroundColor = [UIColor eiFadedRedColor];
            self.displayAllCommentsButton.hidden = NO;
            NSLog(@"at comments");
            break;
        default:
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.commentsVC.view setFrame:CGRectMake(5, 5, self.view.frame.size.width - 10, 310)];
  //  [self.similarProductsVC.view setFrame:CGRectMake(5, 5, self.view.frame.size.width - 5, 240)];
    if (self.commentsVC != nil) {
        [self.commentsVC.tableView reloadData];

    }
    if (self.similarProductsVC != nil) {
        [self.similarProductsVC.collectionView reloadData];

    }
}

- (void)setupSimilarProductsView {
//    self.similarProductsView = [[UIView alloc] initWithFrame:CGRectMake(0, self.segmentedControl.frame.origin.y + self.segmentedControl.frame.size.height, self.view.frame.size.width, 240)];
//    self.similarProductsView.hidden = NO;
//    self.similarProductsVC = [[SimilarProductsCollectionViewController alloc] initWithNibName:@"SimilarProductsCollectionViewController" bundle:nil];
//    [self.similarProductsView addSubview:self.similarProductsVC.view];
//    [self.itemScroll addSubview:self.similarProductsView];

}

- (void)setupCommentsView {
    self.commentsView = [[UIView alloc] init];
    [self.commentsView setFrame:CGRectMake(0, self.segmentedControl.frame.origin.y + self.segmentedControl.frame.size.height, self.view.frame.size.width, 310)];
    self.commentsView.hidden = YES;
    self.commentsVC = [[ProductCommentsViewController alloc] initWithNibName:@"ProductCommentsViewController" bundle:nil];

    [self.commentsView addSubview:self.commentsVC.view];
    [self.itemScroll addSubview:self.commentsView];
    [self.commentsVC.view setFrame:CGRectMake(5, 5, self.view.frame.size.width - 10, 310)];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return contentDetail.items.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.adManager shouldShowAdAtIndexPath:indexPath]){
        return [self.adManager heightForRowAtIndexPath:indexPath];
    }
    return 215;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self.adManager shouldShowAdAtIndexPath:indexPath]){
        return [self.adManager cellForRowAtIndexPath:indexPath];
    }    
  
    SaleItemView* cell = [tableView dequeueReusableCellWithIdentifier:@"saleItemCell"];
    
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"SaleItemView" bundle:nil] forCellReuseIdentifier:@"saleItemCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"saleItemCell"];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1]];
    cell.contentView.userInteractionEnabled = NO;
    
    Product* item = [contentDetail.items objectAtIndex:indexPath.row];
    if(item.itemIsSale && item.itemProductId.length != 0) {
        
        ThirdPartyProduct* tpp = [ObjectMapper getThirdPartProductById:item.itemProductId];
        
        if(tpp==nil){
            [cell setHidden:YES];
            return cell;
        }
        
        [app ganSendAction:@"Detay-Product" withAction:tpp.type withLabel:[NSString stringWithFormat:@"%@",contentDetail.contentTitle]];
        
        cell.sizes = tpp.items;
        cell.pickerLabel = tpp.dizi;
        cell.itemInfo = tpp.itemDescription;
        cell.labelBrand.text = item.itemBrand.length !=0 ? item.itemBrand : tpp.brand;
        cell.labelName.text = item.itemName.length !=0 ? item.itemName : tpp.name;
        [cell.imageProduct sd_setImageWithURL:[NSURL URLWithString:item.itemImage.length !=0 ? item.itemImage : tpp.image] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];
        
        //AFTER HB
        item.itemType = tpp.type;
        item.selectedCatalogName = tpp.catalogName;
        
        if(tpp.isParent && tpp.stock && tpp.parentActive){
            item.selectedProductId = tpp.productId;
        }
        
        cell.labelPrice.text = [NSString stringWithFormat:@"%@ ₺",item.itemPrice.length != 0 ? item.itemPrice : tpp.price];
        cell.labelPrice.font = [UIFont italicSystemFontOfSize:17.0f];
        
        [cell changeItemSaleStatus:YES];
        
        if(tpp.specialPrice != (id)[NSNull null] && [tpp.specialPrice length] != 0){
            cell.labelSpecialPrice.text = [NSString stringWithFormat:@"%@ ₺",tpp.specialPrice];
            cell.labelSpecialPrice.font = [UIFont italicSystemFontOfSize:18.0f];
            
            NSAttributedString * title =
            [[NSAttributedString alloc] initWithString:cell.labelPrice.text
                                            attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
            [cell.labelPrice setAttributedText:title];
            
        }else{
            [cell.labelSpecialPrice setHidden:YES];
        }
        
        if([cell.sizes count] == 0){
            [cell disableItemSizes];
        }
        else{
            [cell enableItemSizes];
        }
        
        if([cell.itemInfo length] == 0){
            [cell disableInfo];
        }
    }
    else{
      
        [cell.imageProduct sd_setImageWithURL:[NSURL URLWithString:item.itemImage] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];
        cell.labelBrand.text = item.itemBrand;
        cell.labelName.text = item.itemName;
        
        if([item.itemPrice length]>0){
            cell.labelPrice.text = [NSString stringWithFormat:@"%@ ₺",item.itemPrice];
            cell.labelPrice.font = [UIFont italicSystemFontOfSize:17.0f];
        }
        else
            cell.labelPrice.text = @"";
            
        [cell changeItemSaleStatus:NO];
        
        if([item.itemStoreLink length] != 0){
            [cell.btnWebPage setHidden:NO];
        }
    }
    
    cell.product = item;
    cell.delegate = self;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.adManager shouldShowAdAtIndexPath:indexPath]){
        [self.adManager didSelectRowAtIndexPath:indexPath];
    }
}


-(IBAction)chooseSizeClicked:(id)sender{
    ProductSize* size = [sizes objectAtIndex:[pickerSize selectedRowInComponent:0]];
   
    if(size.stock <= 0 || size.childActive == 0){
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Ekranda"
                                                         message:[NSString stringWithFormat:@"%@ stokları tükenmiştir..",size.size]
                                                        delegate:self
                                               cancelButtonTitle:@"Tamam"
                                               otherButtonTitles: nil];
        [alert show];
    }
    else{
        
        currentSIV.product.selectedProductId = size.productId;
        [currentSIV changeSelectedProduct:size.size];
        
        
        if([currentSIV.product.itemType isEqualToString:@"hepsiburada"])
            [self updateCellSizeChanged:size withView:currentSIV];
        
        currentSIV = nil;
        sizes = nil;
        NSLog(@"selected product for x %@",size.productId);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        pickerContainer.alpha = 0;
    } completion:^(BOOL finished) {
        
        [pickerContainer setHidden:YES];
    }];
}

-(void)updateCellSizeChanged:(ProductSize *)size withView:(SaleItemView *)cell{
   
    if([size.image length] > 0){
        [cell.imageProduct sd_setImageWithURL:[NSURL URLWithString:size.image] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];
    }
    
    if([size.productName length] > 0){
        cell.labelName.text = size.productName;
    }
    
    if([size.productPrice length] > 0){
        cell.labelPrice.text = [NSString stringWithFormat:@"%@ ₺",size.productPrice];
    }
}

- (void)buttonSizesClicked:(id)selfObject{
    currentSIV = (SaleItemView *)selfObject;
    sizes = currentSIV.sizes;
    [self.pickerLabel setText:currentSIV.pickerLabel];
    
    [pickerSize reloadAllComponents];
    [pickerContainer setHidden:NO];
    pickerContainer.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        pickerContainer.alpha = 1;
    } completion:^(BOOL finished) {
        NSLog(@"picker displayed");
    }];

}

- (void)buttonAddToFavoritesClicked:(id)selfObject{
    SaleItemView* cs = (SaleItemView *)selfObject;
    [app saveToFavorites:cs.product];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Favoriler" message:[NSString stringWithFormat:@"%@-%@ başarıyla favorilere eklendi.",cs.labelName.text,cs.labelBrand.text]
                                                   delegate:self cancelButtonTitle:nil otherButtonTitles:@"Tamam", nil];
    [alert show];
  
    [cs.favButton setImage:[UIImage imageNamed:@"btnUrunFav"] forState:UIControlStateNormal];
    //[TapTarget ttlog:[NSString stringWithFormat:@"%@/favorite",tapPath] withValue:cs.labelName.text andScore:2];
    
    [app ganSendAction:@"favorite" withAction:@"add" withLabel:[NSString stringWithFormat:@"%@-%@",cs.labelName.text,cs.labelBrand.text]];
}

- (void)buttonShareClicked:(id)selfObject{
    
    currentSIV = (SaleItemView *)selfObject;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Paylaş"
                                                             delegate:self
                                                    cancelButtonTitle:@"Vazgeç"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Facebook", @"Twitter", @"WhatsApp" , @"Instagram" , @"Email" , nil];

    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)buttonWebPageClicked:(id)selfObject{
    SaleItemView* cs = (SaleItemView *)selfObject;
    NSLog(@"go url for tihs %@",cs.product.itemStoreLink);
    
    [app ganSendAction:@"sepet" withAction:@"webpage" withLabel:cs.product.itemStoreLink];
    
    //[TapTarget ttlog:[NSString stringWithFormat:@"%@/webpage",tapPath] withValue:cs.product.itemStoreLink andScore:3];
    
    BrowserViewController* bc = [[BrowserViewController alloc] init];
    bc.isWebPage = YES;
    [bc loadProductUrl:cs.product.itemStoreLink];
    [self.navigationController pushViewController:bc animated:YES];
}

- (void)buttonAddToCardClicked:(id)selfObject{
 
    SaleItemView* current = (SaleItemView*)selfObject;
    if([current.product.selectedProductId length] <= 0 && [current.sizes count] > 0){
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Ekranda"
                                                         message:@"Lütfen seçenek belirtiniz.."
                                                        delegate:self
                                               cancelButtonTitle:@"Tamam"
                                               otherButtonTitles: nil];
        [alert show];
        return;
    }
    else if([current.sizes count] <= 0 && [current.product.selectedProductId length] <=0){
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Ekranda"
                                                         message:@"Ürünün stokları tükenmiştir.Lütfen daha sonra tekrar deneyiniz."
                                                        delegate:self
                                               cancelButtonTitle:@"Tamam"
                                               otherButtonTitles: nil];
        [alert show];
        return;
    }
    else{
        NSString* dimStr = [NSString stringWithFormat:@"%@ - %@\rSepete Ekleniyor..." , current.labelBrand.text,current.labelName.text];
        [Core addLoading:self.view withDim:YES dimString:dimStr];
        
        if([current.product.itemType isEqualToString:@"hepsiburada"]){
            
            NSDictionary *productArgs = [NSDictionary dictionaryWithObjectsAndKeys:
                                   current.product.selectedCatalogName, @"catalogName",
                                   current.product.selectedProductId, @"SKU"
                                         ,nil];
            [self performSelectorInBackground:@selector(addCardHB:) withObject:productArgs];
        }
        else{
            [self performSelectorInBackground:@selector(addCard:) withObject:current.product.selectedProductId];
        }
        
    }
}

-(void)addCard:(NSString*)productId{
    
    BOOL result = [JsonParser addToCard:productId];
    [app ganSendAction:@"sepet" withAction:@"add" withLabel:productId];
    
    //[TapTarget ttlog:[NSString stringWithFormat:@"%@/sepet",tapPath] withValue:productId andScore:3];
    
    NSDictionary* goDictionary  = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSNumber numberWithBool:result], @"result",
                                   [NSNumber numberWithInt:s_GO_LIDYANA], @"type"
                                   ,nil];
    
    [self performSelectorOnMainThread:@selector(addToCardFinish:) withObject:goDictionary waitUntilDone:NO];
}

-(void)addCardHB:(NSDictionary*)productArgs{
    
    BOOL resultBool = NO;
    NSDictionary* result = [JsonParser addToCard:[productArgs objectForKey:@"SKU"] catalogName:[productArgs objectForKey:@"catalogName"] quantity:@"1" secureToken:app.hbSecureObject];
    
    if(result != nil){
        resultBool = YES;
        [app saveHBObject:result];
    }
    
    [app ganSendAction:@"sepet-hepsiburada" withAction:@"add" withLabel:[productArgs objectForKey:@"SKU"]];
    
    //[TapTarget ttlog:[NSString stringWithFormat:@"%@/sepet-hepsiburada",tapPath] withValue:[productArgs objectForKey:@"SKU"] andScore:3];
    
    NSDictionary* goDictionary  = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              [NSNumber numberWithBool:resultBool], @"result",
                                                                [NSNumber numberWithInt:S_GO_HEPSIBURADA], @"type"
                                                               ,nil];
    
    [self performSelectorOnMainThread:@selector(addToCardFinish:) withObject:goDictionary waitUntilDone:NO];
}

-(void)addToCardFinish:(NSDictionary *)goDictionary{
    [Core removeLoading:self.view];
    
    
    BOOL resultBool = [[goDictionary objectForKey:@"result"] boolValue];
    NSString* resultString;
    if(resultBool)
        resultString = @"Ürün başarıyla sepete eklendi";
    else
        resultString = @"Bir hata oluştu , lütfen tekrar deneyiniz.";
    
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Ekranda"
                                                     message:resultString
                                                    delegate:self
                                           cancelButtonTitle:@"Tamam"
                                           otherButtonTitles: nil];
    
    [alert setTag:[[goDictionary objectForKey:@"type"] integerValue]];
    [alert addButtonWithTitle:@"Sepete Git"];
    [alert show];
}


- (NSMutableAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    ProductSize* size = [sizes objectAtIndex:row];
    NSMutableAttributedString *attString;
    if(size.stock <= 0 || size.childActive == 0){
    
        attString = [[NSMutableAttributedString alloc] initWithString:size.size];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0,[attString length])];
        
        [attString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0,[attString length])];
        
        [attString addAttribute:NSStrikethroughColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,[attString length])];
    }
    else{
        attString = [[NSMutableAttributedString alloc] initWithString:size.size];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,[attString length])];
    }
    
    return attString;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return  sizes.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0){
        NSLog(@"You have clicked Cancel");
    }
    else if(buttonIndex == 1){
        BrowserViewController *bc = [[BrowserViewController alloc] init];
        bc.goType = alertView.tag;
        [bc loadUrl];
        [self.navigationController pushViewController:bc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.view layoutIfNeeded];
}


- (void)parallaxView:(APParallaxView *)view willChangeFrame:(CGRect)frame {
    // Do whatever you need to do to the parallaxView or your subview before its frame changes
    NSLog(@"parallaxView:willChangeFrame: %@", NSStringFromCGRect(frame));
}

- (void)parallaxView:(APParallaxView *)view didChangeFrame:(CGRect)frame {
    // Do whatever you need to do to the parallaxView or your subview after its frame changed
    NSLog(@"parallaxView:didChangeFrame: %@", NSStringFromCGRect(frame));
}

-(void)adViewDidLoad:(SLAdView*)adView{
    NSLog(@"ad load success");
    [self.view addSubview:self.adView];
    [self.view bringSubviewToFront:self.pickerContainer];
}

-(void)adViewDidFailToLoad:(SLAdView*)adView{
    if ([self.view.subviews containsObject:self.adView]) {
        [self.adView removeFromSuperview];
    }
}

-(void)interstitialDidLoad:(MPInterstitialAdController *)interstitial{
    [interstitial showFromViewController:self];
}

-(void)interstitialDidFailToLoad:(MPInterstitialAdController *)interstitial{
    NSLog(@"interstitial fail");
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.intAdManager.delegate = nil;
    [self.adManager clean];
    self.adManager = nil;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString* shareText = [NSString stringWithFormat:@"Ekranda uygulamasında %@-%@ ürününü beğendim.. Sen de hemen indir ! %@ ",currentSIV.labelBrand.text, currentSIV.labelName.text, @"https://itunes.apple.com/us/app/ekranda/id943898952?ls=1&mt=8"];
    
 //   NSString* shareText = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/ekranda/id943898952"];
    
    switch (buttonIndex) {
            
        case 0 :
        {
           SLComposeViewController* mySLComposerSheet = [[SLComposeViewController alloc] init];
           mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
          [mySLComposerSheet setInitialText:shareText];
          [mySLComposerSheet addImage:currentSIV.imageProduct.image];
          [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            
            
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"iptal edildi");
                        break;
                    case SLComposeViewControllerResultDone:
                        NSLog(@"başarılı");
                        
                        [app ganSendAction:@"share"
                                     withAction:@"facebook"
                                      withLabel:[NSString stringWithFormat:@"%@-%@",currentSIV.labelName.text,currentSIV.labelBrand.text]];
                        
                         //[TapTarget ttlog:[NSString stringWithFormat:@"%@/share/facebook",tapPath] withValue:currentSIV.labelName.text andScore:4];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Mesajınız başarıyla paylaşıldı..." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                        [alert show];
                        break;
                }
            }];
            
          break;
        }
        case 1 :
        {
            SLComposeViewController *tweetSheetOBJ = [SLComposeViewController
                                                      composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheetOBJ setInitialText:shareText];
            [self presentViewController:tweetSheetOBJ animated:YES completion:nil];
            
            [tweetSheetOBJ setCompletionHandler:^(SLComposeViewControllerResult result) {
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"iptal edildi");
                        break;
                    case SLComposeViewControllerResultDone:
                        NSLog(@"başarılı");
                        
                        [app ganSendAction:@"share"
                                     withAction:@"twitter"
                                      withLabel:[NSString stringWithFormat:@"%@-%@",currentSIV.labelName.text,currentSIV.labelBrand.text]];
                        
                         //[TapTarget ttlog:[NSString stringWithFormat:@"%@/share/twitter",tapPath] withValue:currentSIV.labelName.text andScore:4];
                
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Mesajınız başarıyla paylaşıldı..." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                        [alert show];
                        break;
                }
            }];
            break;
        }
        case 2 :
        {
            NSString * text = [shareText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            text = [text stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
            text = [text stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
            text = [text stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
            text = [text stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
            text = [text stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
            text = [text stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
            
            NSString *textToSend = [NSString stringWithFormat:@"whatsapp://send?text=%@",text];
            NSURL *whatsappURL = [NSURL URLWithString:textToSend];
            
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
                [[UIApplication sharedApplication] openURL: whatsappURL];
                
                //[TapTarget ttlog:[NSString stringWithFormat:@"%@/share/whatsapp",tapPath] withValue:currentSIV.labelName.text andScore:4];
                [app ganSendAction:@"share"
                             withAction:@"whatsapp"
                              withLabel:[NSString stringWithFormat:@"%@-%@",currentSIV.labelName.text,currentSIV.labelBrand.text]];
                
            }else{
                [[[UIAlertView alloc] initWithTitle:nil message:@"WhatsApp bu cihazda yüklü değil." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil, nil] show];
            }
            break;
        }
        case 4:
        {
            if([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
                mailCont.mailComposeDelegate = self;
                [mailCont setSubject:@"Ekranda"];
                [mailCont setMessageBody:shareText isHTML:NO];
                
                //[TapTarget ttlog:[NSString stringWithFormat:@"%@/share/email",tapPath] withValue:currentSIV.labelName.text andScore:4];
                
                [app ganSendAction:@"share"
                             withAction:@"email"
                              withLabel:[NSString stringWithFormat:@"%@-%@",currentSIV.labelName.text,currentSIV.labelBrand.text]];
                
                [self presentViewController:mailCont animated:YES completion:nil];
            }
            break;
        }
        case 3:
        {
            NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
            if([[UIApplication sharedApplication] canOpenURL:instagramURL])
            {
                CGFloat cropVal = (currentSIV.imageProduct.image.size.height > currentSIV.imageProduct.image.size.width ? currentSIV.imageProduct.image.size.width : currentSIV.imageProduct.image.size.height);
                
                cropVal *= [currentSIV.imageProduct.image scale];
                
                CGRect cropRect = (CGRect){.size.height = cropVal, .size.width = cropVal};
                CGImageRef imageRef = CGImageCreateWithImageInRect([currentSIV.imageProduct.image CGImage], cropRect);
                
                NSData *imageData = UIImageJPEGRepresentation([UIImage imageWithCGImage:imageRef], 1.0);
                CGImageRelease(imageRef);
                
                NSString *writePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"instagram.igo"];
                if (![imageData writeToFile:writePath atomically:YES]) {
                    // failure
                    NSLog(@"image save failed to path %@", writePath);
                    return;
                } else {
                    // success.
                }
                
                // send it to instagram.
                NSURL *fileURL = [NSURL fileURLWithPath:writePath];
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
                self.documentController.delegate = self;
                [self.documentController setUTI:@"com.instagram.exclusivegram"];
                [self.documentController setAnnotation:@{@"InstagramCaption" : shareText}];
                [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 320, 480) inView:self.view animated:YES];
                
                [app ganSendAction:@"share"
                             withAction:@"instagram"
                              withLabel:[NSString stringWithFormat:@"%@-%@",currentSIV.labelName.text,currentSIV.labelBrand.text]];
                
                //[TapTarget ttlog:[NSString stringWithFormat:@"%@/share/instagram",tapPath] withValue:currentSIV.labelName.text andScore:4];
            }
            else
            {
               [[[UIAlertView alloc] initWithTitle:nil message:@"Instagram bu cihazda yüklü değil." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil, nil] show];
            }
            break;
        }
        case 5:
        {
            break;
        }
    }
    
}

- (void)buttonZoomClicked:(id)selfObject{
    SaleItemView* temp = (SaleItemView *)selfObject;
    if (self.zoomedIndexPath == nil) {
        [self zoomImageAtIndexPath:temp];
    }
}

- (void)zoomImageAtIndexPath:(SaleItemView *)cell {
    [self.zoomedImageView setImage:cell.imageProduct.image withFrame:[cell.imageProduct convertRect:cell.imageProduct.bounds toView:self.zoomedImageView.superview]];
    [self.zoomedImageView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.zoomedImageView];
    cell.imageProduct.hidden = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.zoomedImageView.frame = self.view.bounds;
    }];
    self.itemScroll.panGestureRecognizer.enabled = NO;
    self.zoomedIndexPath = [self.tableView indexPathForCell:cell];
}

@synthesize zoomedImageView = _zoomedImageView;

- (ImageWithZoom *)zoomedImageView {
    if (!_zoomedImageView) {
        _zoomedImageView = [[ImageWithZoom alloc] init];
        _zoomedImageView.contentMode = UIViewContentModeScaleAspectFit;
        _zoomedImageView.clipsToBounds = YES;
        _zoomedImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewWasTapped)];
        [_zoomedImageView addGestureRecognizer:tapRecognizer];
    }
    return _zoomedImageView;
}

- (void)imageViewWasTapped {
    SaleItemView *cell = (SaleItemView *)[self.tableView cellForRowAtIndexPath:self.zoomedIndexPath];
    [UIView animateWithDuration:0.3 animations:^{
        self.zoomedImageView.frame = [cell.imageProduct convertRect:cell.imageProduct.bounds toView:self.zoomedImageView.superview];
    } completion:^(BOOL finished) {
        [self.zoomedImageView removeFromSuperview];
        cell.imageProduct.hidden = NO;
        self.zoomedIndexPath = nil;
        [self.zoomedImageView unsetImage];
        self.itemScroll.panGestureRecognizer.enabled = YES;
    }];
}



@end
