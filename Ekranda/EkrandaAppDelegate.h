//
//  AppDelegate.h
//  Ekranda
//
//  Created by Mesut Dağ on 10/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"

#import "MenuCategory.h"
#import "Product.h"
#import "ios-ntp.h"

//#import <TapTarget/TapTarget.h>

#define k_PUSHNOTIFICATION_REGISTRATION @"http://213.243.33.176/MANS/enroll.aspx?deviceId="
#define k_PUSHNOTIFICATION_APP_ID  @"appId=16"
#define k_PUSHNOTIFICATION_DEVICE_TYPE  @"deviceType=2"

@interface EkrandaAppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong)  MMDrawerController* drawerController;
@property (nonatomic,strong) UIView* notifView;
@property (nonatomic,strong) UIView* html5View;


@property (strong, nonatomic) NSMutableArray *futureContents;
@property (strong, nonatomic) NSMutableArray *categories;
@property (strong, nonatomic) NSMutableArray *products;
@property (strong, nonatomic) NSMutableArray *hbProducts;
@property (strong, nonatomic) NSMutableArray *favorites;
@property (strong, nonatomic) NSDictionary *hbSecureObject;

-(void)showHome:(MenuCategory *)category withToggle:(BOOL)toggle;
-(void)addNewContentNotification;
-(void)addHTMLContent:(NSString *)url;
-(void)startDetector;
-(void)stopDetector;
-(void)saveToFavorites:(Product *)content;
-(void)loadHBObject;
-(void)saveHBObject:(NSDictionary *)dictionary;
-(void)removeFromFavorites:(int)itemId;

-(void)ganSendView:(NSString *)screenName;
-(void)ganSendAction:(NSString *)category withAction:(NSString *)action withLabel:(NSString *)label;
-(BOOL) needsUpdate;
-(void)checkAndOpenMicrophone;

@end

