//
//  ObjectMapper.h
//  Ekranda
//
//  Created by Mesut Dağ on 30/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Content.h"
#import "ThirdPartyProduct.h"

@interface ObjectMapper : NSObject

+(Content *)getFutureContentByWatermark:(long long) timeStamp;
+(ThirdPartyProduct *)getThirdPartProductById:(NSString *)productId;

@end
