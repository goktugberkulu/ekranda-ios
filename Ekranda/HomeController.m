//
//  AnasayfaController.m
//  Ekranda
//
//  Created by Mesut Dağ on 13/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "HomeController.h"
#import "JsonParser.h"
#import "Content.h"
#import "UICard.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "DetailController.h"
#import "Core.h"
#import "BrowserViewController.h"
#import "MenuController.h"
#import "Reachability.h"
#import "WalkThrough.h"

@interface HomeController()
   @property (assign, nonatomic) CATransform3D initialTransformation;
@end

@implementation HomeController

@synthesize mainTable,contents;

int pageNumber = 1;
NSInteger lastContentSize;
BOOL isCheckedForceUpdate = YES;

-(void)loadUI{
    NSLog(@"LOAD UI");
    contents = [JsonParser getContents:self.category withPagaNumber:pageNumber];
    lastContentSize = contents.count;
    [self performSelectorOnMainThread:@selector(fillTable) withObject:nil waitUntilDone:NO];
}

-(void)loadAds{
    self.adManager = [[SLTableViewAdManager alloc] initWithTableView:self.mainTable
                                                          dataSource:contents
                                                      viewController:self];
    
    NSString* entityName = self.category ?  self.category.categoryTitle : @"anasayfa";
    
    [self.adManager
     getAdsWithEntity:entityName
     category:SLCategoryFashion
     successBlock:^{
         NSLog(@"ads loaded");
     }
     failureBlock:^(NSError *error) {
         NSLog(@"%@",error);
     }];
}

-(void)fillTable{
    [self.mainTable setHidden:NO];
    [Core removeLoading:self.view];
    
    self.mainTable.dataSource = self;
    self.mainTable.delegate = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainTable reloadData];
    });
    
    [self.refreshControl endRefreshing];
    [self updateRefreshTitle];
    [self loadAds];
}

-(void)updateRefreshTitle{
   
    if(self.refreshControl){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Son Güncelleme: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor colorWithRed:242.0/255.0 green:91.0/255.0 blue:90.0/255.0 alpha:1]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CATransform3D transform = CATransform3DIdentity;
    transform = CATransform3DScale(transform, 0.7, 0.7, 0.7);
    self.initialTransformation = transform;
    
    [self.mainTable setHidden:YES];
    [Core addLoading:self.view withDim:NO dimString:nil];
    
    [self performSelectorInBackground:@selector(loadUI) withObject:nil];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.mainTable;
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor colorWithRed:242.0/255.0 green:91.0/255.0 blue:90.0/255.0 alpha:1];
    [self.refreshControl addTarget:self action:@selector(refreshTableWithControl) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.refreshControl beginRefreshing];
        [self.refreshControl endRefreshing];
    });
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newContentTrigger:)
                                                 name:@"newContent" object:nil];
}

-(void)loadMore{
    
    if(lastContentSize<100)
        return;

    pageNumber++;
    NSLog(@"getting more content for page %d",pageNumber);
    NSMutableArray* moreContents = [JsonParser getContents:self.category withPagaNumber:pageNumber];
    lastContentSize = moreContents.count;
    [contents addObjectsFromArray:moreContents];
    [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
}

-(void)refreshTableWithControl{
    NSLog(@"refreshing table");
    pageNumber = 1;
    [self performSelectorInBackground:@selector(loadUI) withObject:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSString* entityName = self.category ?  self.category.categoryTitle : @"Anasayfa";
    EkrandaAppDelegate *delegate = (EkrandaAppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate ganSendView:entityName];
    if (isCheckedForceUpdate) {
        isCheckedForceUpdate = NO;
        [NSThread detachNewThreadSelector:@selector(checkUpdate:) toTarget:self withObject:delegate];
    }
    
    //[TapTarget ttlog:[NSString stringWithFormat:@"/%@",entityName] withValue:@"" andScore:1];
}

-(void) checkUpdate:(EkrandaAppDelegate*)delege {
    BOOL isNeedsUpdate = [delege needsUpdate];
    
    if (isNeedsUpdate) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ekranda" message:@"Uygulamanın yeni versiyonunu indirmeniz gerekmektedir." delegate:self cancelButtonTitle:@"İndir" otherButtonTitles:nil];
            [alert show];
        });
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1453) {
        alert = nil;
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/tr/app/ekranda/id943898952?mt=8"]];
    exit(0);
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return contents.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.adManager shouldShowAdAtIndexPath:indexPath])
    {
        return [self.adManager heightForRowAtIndexPath:indexPath];
    }
    return (IS_IPHONE_6 || IS_IPHONE_6P) ? 240 : 205;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if([self.adManager shouldShowAdAtIndexPath:indexPath]){
        return [self.adManager cellForRowAtIndexPath:indexPath];
    }
    
    UICard* cell = [tableView dequeueReusableCellWithIdentifier:@"cardCell"];
    
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"UICard" bundle:nil] forCellReuseIdentifier:@"cardCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cardCell"];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.contentView.userInteractionEnabled = NO;
    Content* current = [contents objectAtIndex:indexPath.row];
    
    UIView *cardContatiner = [cell container];
    
    cardContatiner.layer.transform = self.initialTransformation;
    
    cardContatiner.layer.opacity = 0.7;
    
    [UIView animateWithDuration:0.5 animations:^{
        cardContatiner.layer.transform = CATransform3DIdentity;
        cardContatiner.layer.opacity = 1;
    }];

    if (current.contentIsHTML5 && [[current.contentHTML5Type lowercaseString] isEqualToString:@"content"]) {
        [cell.container setHidden:YES];
        [cell.html5View setHidden:NO];
        
        cell.html5View.scrollView.scrollEnabled = NO;
        cell.html5View.scrollView.bounces = NO;
        
        [cell.html5View loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:current.contentHTML5Url]]];
        cell.html5View.delegate = self;
        
        return cell;
    }
    [cell.container setHidden:NO];
    [cell.html5View setHidden:YES];
    if(current.isNew){
        [cell.iconImageNew setHidden:NO];
        [cell.container setBackgroundColor:[UIColor colorWithRed:242.0/255.0 green:91.0/255.0 blue:90.0/255.0 alpha:1]];
    }
    else{
        [cell.iconImageNew setHidden:YES];
        [cell.container setBackgroundColor:[UIColor whiteColor]];
    }
    
    cell.contentId = current.contentId;
    [cell.mainText setText:current.contentTitle];
    [cell.likeLabel setText:[NSString stringWithFormat:@"%d",current.contentLikeCount]];
    [cell.mainImage sd_setImageWithURL:[NSURL URLWithString:current.contentImgUrl]
                      placeholderImage:[UIImage imageNamed:@"stub.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          if (error && !alert) {
                              alert = [[UIAlertView alloc] initWithTitle:@"Ekranda" message:@"İnternet bağlantısı koptu" delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                              alert.tag = 1453;
                              [alert show];
                          }
                      }];
    
    [cell.likeImage setImage:[UIImage imageNamed:@"star_empty_ico@2X.png"]];
    
    return cell;
}

UIAlertView* alert = nil;

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
  
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex) && lastContentSize>=100){
        
        [Core addLoading:self.view withDim:NO dimString:nil];
        [self performSelectorInBackground:@selector(loadMore) withObject:nil];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bağlantı hatası"
                                                        message:@"Bir hata oluştu. Lütfen internet bağlantınızı kontrol edin."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if([self.adManager shouldShowAdAtIndexPath:indexPath]){
        [self.adManager didSelectRowAtIndexPath:indexPath];
    }
    else{
        Content* current = [contents objectAtIndex:indexPath.row];
        DetailController* dc = [[DetailController alloc] init];
        dc.content = current;
        dc.category = self.category;
        [self.navigationController pushViewController:dc animated:YES];
    }
}

-(BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        BrowserViewController* bc = [[BrowserViewController alloc] init];
        bc.isWebPage = YES;
        [bc loadProductUrl:[request.URL absoluteString]];
        [self.navigationController pushViewController:bc animated:YES];
        
        return NO;
    }
    
    return YES;
}

-(void)scrollToTop{

    [self.mainTable scrollRectToVisible:CGRectMake(0, 0+self.refreshControl.frame.size.height, 1, 1) animated:YES];
}

-(void)reloadTable{
    [self.mainTable reloadData];
    [Core removeLoading:self.view];
    
}

- (BOOL)isVisible {
    return [self isViewLoaded] && self.view.window;
}

- (BOOL)isAtTop {
    return (self.mainTable.contentOffset.y == 0);
}

- (void)newContentTrigger:(NSNotification *)note {
    
    if([note.object isKindOfClass:[Content class]]){
        [self.contents insertObject:note.object atIndex:0];
        [self.mainTable reloadData];
    }
    
    NSLog(@"Received Notification - Someone seems to have logged in");
    
}

@end
