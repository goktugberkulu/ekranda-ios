//
//  FavoriteViewController.h
//  Ekranda
//
//  Created by Mesut Dağ on 18/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "SaleItemView.h"

@interface FavoriteViewController : CustomViewController<UITableViewDataSource,UITableViewDelegate,SaleItemViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView* itemScroll;
@property (strong, nonatomic) NSMutableArray *favorites;

@end
