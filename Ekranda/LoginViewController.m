//
//  LoginViewController.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "LoginViewController.h"
#import "UIColor+Ekranda.h"
#import "UIFont+Ekranda.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *forgetPasswordButton;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
 
    [self setFieldBorderForOnlyBottom:self.usernameField withColor:[UIColor eiWhiteColor]];
    [self setFieldBorderForOnlyBottom:self.passwordField withColor:[UIColor eiWhiteColor]];
    
    
    self.forgetPasswordButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    self.registerButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLayoutSubviews {
   self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}


- (void)setFieldBorderForOnlyBottom:(UITextField *)field withColor:(UIColor *)color {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = color.CGColor;
    border.frame = CGRectMake(0, field.frame.size.height - borderWidth, field.frame.size.width, 1);
    border.borderWidth = borderWidth;
    [field.layer addSublayer:border];
    field.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)userEnter:(UIButton *)sender {

    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self presentViewController:app.drawerController animated:NO completion:nil];

}

- (IBAction)connectWithTwitter:(UIButton *)sender {
}

- (IBAction)connectWithFacebook:(UIButton *)sender {
}

- (IBAction)forgotPassword:(UIButton *)sender {
}

- (IBAction)signUp:(UIButton *)sender {
}


@end
