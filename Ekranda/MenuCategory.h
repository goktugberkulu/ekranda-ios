//
//  Category.h
//  Ekranda
//
//  Created by Mesut Dağ on 14/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuCategory : NSObject

@property (nonatomic) int categoryId;
@property (nonatomic, strong) NSString* categoryType;
@property (nonatomic, strong) NSString* categoryTitle;
@property (nonatomic, strong) NSString* categoryUrl;


@end
