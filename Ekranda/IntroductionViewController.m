//
//  IntroductionViewController.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "IntroductionViewController.h"
#import "IntroductionContentViewController.h"
#import "UIColor+Ekranda.h"

@interface IntroductionViewController ()

@end

@implementation IntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.pageHeaderTexts = @[@"Ekranda nedir? Nasıl çalışır?", @"Ekranda Ana Sayfa", @"Ürün Detay Sayfası", @"Sepetim"];
    self.pageDetailTexts = @[@"Ekranda, dizi kıyafet marka arşiv özelliği yanı sıra TV ile senkronize anlık yeni ürün bilgisi verebilmektedir.", @"Detay bilgisi verilen ürün etiket ikonu ile belirtilir. Aynı zamanda dizi sahnesi olan bu görsele dokunduğunuzda ürün detay sayfasına yönlenirsiniz.", @"Ürün hakkında bilgi edinebilir, sosyal medyada paylaşabilir, favori listesi oluşturabilirsiniz. Eğer ürün satışta ise sadece beden seçimi yapıp kolayca sepete ekleyebilirsiniz.", @"Almak istediğiniz ürünleri sağ üst köşede yer alan sepet ikonuna tıklayarak inceleyebilir ve satın alma işleminizi tamamlayabilirsiniz."];
    self.pageImages = @[@"imgIntro1.png", @"imgIntro2.png", @"imgIntro3.png", @"imgIntro4.png"];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    IntroductionContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    
}

//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((IntroductionContentViewController *) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((IntroductionContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageHeaderTexts count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (IntroductionContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageHeaderTexts count] == 0) || (index >= [self.pageHeaderTexts count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    IntroductionContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroductionContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.headerText = self.pageHeaderTexts[index];
    pageContentViewController.detailText = self.pageDetailTexts[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}




- (BOOL)prefersStatusBarHidden {
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
