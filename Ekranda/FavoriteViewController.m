//
//  FavoriteViewController.m
//  Ekranda
//
//  Created by Mesut Dağ on 18/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "FavoriteViewController.h"
#import "EkrandaAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ThirdPartyProduct.h"
#import "DetailController.h"
#import "Content.h"
#import "ObjectMapper.h"

@implementation FavoriteViewController

@synthesize favorites;

EkrandaAppDelegate *app;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.itemScroll.dataSource = self;
    self.itemScroll.delegate = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.itemScroll reloadData];
    });
    
     app = [[UIApplication sharedApplication] delegate];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    EkrandaAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate ganSendView:@"Favorilerim"];
    //[TapTarget ttlog:@"Favorilerim" withValue:@"" andScore:1];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return favorites.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 215;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SaleItemView* cell = [tableView dequeueReusableCellWithIdentifier:@"saleItemCell"];
    
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"SaleItemView" bundle:nil] forCellReuseIdentifier:@"saleItemCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"saleItemCell"];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1]];
    
    cell.contentView.userInteractionEnabled = NO;
    
    Product* item = [favorites objectAtIndex:(favorites.count - indexPath.section - 1)];
    if(item.itemIsSale && item.itemProductId.length != 0){
        
        ThirdPartyProduct* tpp = [ObjectMapper getThirdPartProductById:item.itemProductId];
        if(tpp==nil)
            tpp = [app.products objectAtIndex:0];
        
        cell.labelBrand.text = item.itemBrand.length !=0 ? item.itemBrand : tpp.brand;
        cell.labelName.text = item.itemName.length !=0 ? item.itemName : tpp.name;
        [cell.imageProduct sd_setImageWithURL:[NSURL URLWithString:item.itemImage.length !=0 ? item.itemImage : tpp.image] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];
        cell.btnSizes.titleLabel.numberOfLines = 0;
        [cell.favButton setImage:[UIImage imageNamed:@"btnUrunFav" ] forState:UIControlStateNormal];
        
        
    }
    else{
        
        [cell.imageProduct sd_setImageWithURL:[NSURL URLWithString:item.itemImage] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"]];
        cell.labelBrand.text = item.itemBrand;
        cell.labelName.text = item.itemName;
        [cell.favButton setImage:[UIImage imageNamed:@"btnUrunFav" ] forState:UIControlStateNormal];
    }
    
    [cell changeItemSaleStatus:NO];
    [cell disableShare];
    [cell.labelFavorite setText:@"Favorilerden Çıkar"];
    [cell.zoomImage setHidden:YES];
    cell.product = item;
    cell.delegate = self;
    return cell;
}

- (void)buttonAddToFavoritesClicked:(id)selfObject{
    SaleItemView* cs = (SaleItemView *)selfObject;
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app removeFromFavorites:cs.product.itemId];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Favoriler" message:[NSString stringWithFormat:@"%@-%@ favorilerinizden silindi...",cs.labelName.text,cs.labelBrand.text]
                                                   delegate:self cancelButtonTitle:nil otherButtonTitles:@"Tamam", nil];
    [alert show];
    [cs.favButton setImage:[UIImage imageNamed:@"btnUrunFavori"] forState:UIControlStateNormal];
    [self.itemScroll reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailController* dc = [[DetailController alloc] init];
    Product *cp = [favorites objectAtIndex:(favorites.count - indexPath.section - 1)];
    Content *current = [[Content alloc] init];
    current.contentId = cp.contentId;
    current.contentTitle = cp.contentTitle;
    dc.isFavorite = YES;
    dc.content = current;
    [self.navigationController pushViewController:dc animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
