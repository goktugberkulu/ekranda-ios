//
//  SearchViewController.m
//  EkrandaUI
//
//  Created by Goktug on 02/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "SearchViewController.h"
#import "UIColor+Ekranda.h"
#import "UIFont+Ekranda.h"
#import "CustomSearchController.h"
#import "CustomSearchBar.h"
#import "FilterViewController.h"
#import "ProductTableViewCell.h"
#import "EkrandaAppDelegate.h"


@interface SearchViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *searchImage;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) CustomSearchController *searchController;
@end

@implementation SearchViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
     [self.tableView registerNib:[UINib nibWithNibName:@"ProductCell" bundle:nil] forCellReuseIdentifier:@"searchResultCell"];
    // Do any additional setup after loading the view.
    

    self.tableView.alpha = 0.0;
    self.resultLabel.alpha = 0.0;
    self.filterButton.alpha = 0.0;
    self.view.backgroundColor = [UIColor grayColor];

    self.searchController = [[CustomSearchController alloc] initWithViewController:self backgroundColor:[UIColor eiShadowFadedRedColor] textColor:[UIColor eiWhiteColor] font:[UIFont systemFontOfSize:14.0] frame:self.navigationItem.titleView.frame];
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
    self.navigationItem.titleView = self.searchController.customSearchBar;
    self.navigationItem.titleView.backgroundColor = [UIColor eiFadedRedColor];
    self.searchController.customSearchBar.delegate = self;
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText length] > 0) {
        
        self.tableView.alpha = 1.0;
        self.resultLabel.alpha = 1.0;
        self.filterButton.alpha = 1.0;
        self.searchImage.alpha = 0.0;
        self.searchLabel.alpha = 0.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    
    ProductTableViewCell *cell = (ProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"searchResultCell"];
    if (cell == nil) {
       
        cell = [tableView dequeueReusableCellWithIdentifier:@"searchResultCell"];
        
    }
    
    return cell;
}

//- (IBAction)filterResults:(UIButton *)sender {
////    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
////    CGRect rect = [keyWindow bounds];
////    UIGraphicsBeginImageContext(rect.size);
////    CGContextRef context = UIGraphicsGetCurrentContext();
////    [keyWindow.layer renderInContext:context];
////    UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
////    UIGraphicsEndImageContext();
////    FilterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterView"];
////    vc.bgImage = screenShot;
////    [self presentViewController:vc animated:NO completion:nil];
//    
//}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (IBAction)closeSearch:(UIBarButtonItem *)sender {
//    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [app showHome:0 withToggle:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"filterSegue"]) {
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect rect = [keyWindow bounds];
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [keyWindow.layer renderInContext:context];
        UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        FilterViewController *vc = segue.destinationViewController;
        vc.bgImage = screenShot;
    }

}


@end
