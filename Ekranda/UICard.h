//
//  UICard.h
//  Ekranda
//
//  Created by Mesut Dağ on 21/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICard : UITableViewCell


@property (nonatomic,strong) IBOutlet UIImageView *mainImage;
@property (nonatomic,strong) IBOutlet UILabel *mainText;
@property (nonatomic, strong) IBOutlet UIView *container;
@property (nonatomic, strong) IBOutlet UIWebView *html5View;
@property (nonatomic,strong) IBOutlet UIImageView *iconImageNew;
@property (nonatomic) int contentId;
@property (nonatomic,strong) IBOutlet UIImageView *likeImage;
@property (nonatomic,strong) IBOutlet UILabel *likeLabel;

+ (NSString*)reuseIdentifier;

@end
