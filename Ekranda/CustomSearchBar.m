//
//  CustomSearchBar.m
//  EkrandaUI
//
//  Created by Goktug on 03/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "CustomSearchBar.h"

@implementation CustomSearchBar


- (instancetype) initWithBackgroundColor:(UIColor *)background textColor:(UIColor *)textColor font:(UIFont *)font frame:(CGRect) frame {
    if (self = [super init]) {
        self.frame = frame;
        self.background = background;
        self.textColor = textColor;
        self.font = font;
        self.searchBarStyle = UISearchBarStyleProminent;
        self.translucent = NO;
        [self sizeToFit];
        
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder*)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}


- (int) indexOfSearchFieldInSubview {
    int index = 0;
    UIView *searchBarView = self.subviews[0];
    
    for (int i=0; i<searchBarView.subviews.count; i++) {
        if ([searchBarView.subviews[i] isKindOfClass:[UITextField class]]) {
            index = i;
            break;
        }
    }
    
    return index;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    int index = [self indexOfSearchFieldInSubview];
    self.barTintColor = [UIColor eiFadedRedColor];
    UITextField *searchField = ((UITextField *) ((UIView *)self.subviews[0]).subviews[index]);
    searchField.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    searchField.font = self.font;
    searchField.backgroundColor = self.background;
    searchField.textColor = self.textColor;
    searchField.textAlignment = NSTextAlignmentLeft;
    searchField.placeholder = @"Ara";
    searchField.leftView = nil;
    searchField.leftView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    ((UIImageView *) searchField.leftView).image = [UIImage imageNamed:@"Search"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor eiWhiteColor];
    placeholderLabel.textAlignment = NSTextAlignmentLeft;
    placeholderLabel.font = self.font;
}

-(void)setShowsCancelButton:(BOOL)showsCancelButton {
    
}

-(void)setShowsCancelButton:(BOOL)showsCancelButton animated:(BOOL)animated {
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}



@end
