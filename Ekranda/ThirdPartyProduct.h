//
//  Product.h
//  Ekranda
//
//  Created by Mesut Dağ on 14/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThirdPartyProduct : NSObject

@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* brand;
@property (nonatomic, strong) NSString* image;
@property (nonatomic) int stock;
@property (nonatomic) int parentActive;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* specialPrice;
@property (nonatomic, strong) NSString* dizi;
@property (nonatomic, strong) NSString* catalogName;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* itemDescription;
@property (nonatomic) BOOL isParent;
@property (nonatomic, strong) NSMutableArray* items;
@property (nonatomic, strong) NSString* type;

@end
