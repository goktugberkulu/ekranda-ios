//
//  ProductCommentsViewController.m
//  EkrandaUI
//
//  Created by Goktug on 15/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "ProductCommentsViewController.h"
#import "ProductCommentsTableViewCell.h"

@interface ProductCommentsViewController ()


@end

@implementation ProductCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductCommentsTableViewCell" bundle:nil] forCellReuseIdentifier:@"commentCell"];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductCommentsTableViewCell *cell = (ProductCommentsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"commentCell"];
    if (cell == nil) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell"];
        
    }
    
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
