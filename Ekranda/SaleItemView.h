//
//  SaleItemView.h
//  Ekranda
//
//  Created by Mesut Dağ on 27/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@protocol SaleItemViewDelegate <NSObject>
- (void)buttonSizesClicked:(id)selfObject;
- (void)buttonAddToCardClicked:(id)selfObject;
- (void)buttonAddToFavoritesClicked:(id)selfObject;
- (void)buttonShareClicked:(id)selfObject;
- (void)buttonZoomClicked:(id)selfObject;
- (void)buttonWebPageClicked:(id)selfObject;
@end

@interface SaleItemView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *favButton;

@property (weak, nonatomic) id <SaleItemViewDelegate> delegate;

@property (nonatomic,strong) IBOutlet UIView *containerView;
@property (nonatomic,strong) IBOutlet UIView *mainView;
@property (nonatomic,strong) IBOutlet UIView *infoView;
@property (nonatomic,strong) IBOutlet UIWebView *infoWebView;

@property (nonatomic,strong) IBOutlet UIImageView *imageProduct;
@property (nonatomic,strong) IBOutlet UIImageView *imageArrow;
@property (nonatomic,strong) IBOutlet UILabel *labelName;
@property (nonatomic, strong) IBOutlet UILabel *labelBrand;
@property (nonatomic, strong) IBOutlet UILabel *labelPrice;
@property (nonatomic, strong) IBOutlet UILabel *labelSpecialPrice;
@property (nonatomic, strong) IBOutlet UIButton *btnAddToCard;
@property (nonatomic, strong) IBOutlet UIButton *btnWebPage;
@property (nonatomic, strong) IBOutlet UIButton *btnSizes;
@property (nonatomic, strong) IBOutlet UIButton *btnInfo;

@property (nonatomic, strong) Product* product;
@property (nonatomic, strong) NSMutableArray* sizes;
@property (nonatomic, strong) NSString* itemInfo;
@property (nonatomic, strong) NSString* pickerLabel;

@property (nonatomic, strong) IBOutlet UILabel *labelInfo;
@property (nonatomic, strong) IBOutlet UIImageView *imageInfo;
@property (nonatomic, strong) IBOutlet UIImageView *zoomImage;
@property (nonatomic, strong) IBOutlet UIButton *zoomButton;

@property (nonatomic, strong) IBOutlet UILabel *labelFavorite;

@property (nonatomic, strong) IBOutlet UIButton *btnShare;
@property (nonatomic, strong) IBOutlet UILabel *shareIco;
@property (nonatomic, strong) IBOutlet UIImageView *shareImage;


-(void)changeSelectedProduct:(NSString*)sizeName;
-(void)changeItemSaleStatus:(BOOL)isSale;
-(void)disableItemSizes;
-(void)disableShare;
-(void)enableItemSizes;
-(void)disableInfo;


- (IBAction)buttonClicked:(id)sender;

@end
