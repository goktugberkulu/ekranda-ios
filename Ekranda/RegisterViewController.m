//
//  RegisterViewController.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "RegisterViewController.h"
#import "UIColor+Ekranda.h"
#import "RegisterTableViewCell.h"

@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet UILabel *userNegLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSArray *labelArray;
@property (strong, nonatomic) NSArray *fieldArray;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setTintColor:[UIColor eiWhiteColor]];
    self.navigationItem.title = @"Üyelik Formu";
    self.labelArray = @[@"Adı", @"Soyadı", @"Kullanıcı Adı", @"E-posta Adresi", @"Şifre", @"Şifre (Tekrar)"];
    self.fieldArray = @[@"John", @"Doe", @"john_doe", @"johndoe@gmail.com", @"******", @"******"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (void)viewWillLayoutSubviews {
//    [self.tableView sizeToFit];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.tableView.frame.size.height / 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RegisterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"registerCell"];
    cell.label.text = [self.labelArray objectAtIndex:indexPath.row];
    cell.field.placeholder = [self.fieldArray objectAtIndex:indexPath.row];
   // [self.tableView sizeToFit];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)viewWillAppear:(BOOL)animated {
     self.navigationController.navigationBar.backItem.title = @"";
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
  //  [self.tableView sizeToFit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
