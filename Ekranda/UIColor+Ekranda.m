//
//  UIColor+Ekranda.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "UIColor+Ekranda.h"

@implementation UIColor (Ekranda)

+ (UIColor *)eiFadedRedColor {
    return [UIColor colorWithRed:225.0 / 255.0 green: 77.0 / 255.0 blue: 82.0 / 255.0 alpha: 1.0];
}

+ (UIColor *)eiWhiteColor {
    return [UIColor colorWithWhite:255.0 / 255.0 alpha: 1.0];
}

+ (UIColor *)eiCharcoalGreyColor {
    return [UIColor colorWithRed:65.0 / 255.0 green: 62.0 / 255.0 blue: 66.0 / 255.0 alpha: 1.0];
}

+ (UIColor *)eiWhiteTwoColor {
    return [UIColor colorWithWhite:217.0 / 255.0 alpha: 1.0];
}

+ (UIColor *)eiDarkSkyBlueColor {
    return [UIColor colorWithRed:74.0 / 255.0 green: 144.0 / 255.0 blue: 226.0 / 255.0 alpha: 1.0];
}

+ (UIColor *)eiLightishPurpleColor {
    return [UIColor colorWithRed:161.0 / 255.0 green: 44.0 / 255.0 blue: 224.0 / 255.0 alpha: 1.0];
}

+ (UIColor *)eiShadowFadedRedColor {
    return [UIColor colorWithRed:187.0/255.0 green:33.0/255.0 blue:38.0/255.0 alpha:1.0];
}

+ (UIColor *)eiGrayColor {
    return [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:114.0/255.0 alpha:1.0];
}

@end
