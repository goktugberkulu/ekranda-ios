//
//  AnasayfaController.h
//  Ekranda
//
//  Created by Mesut Dağ on 13/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import <Seamless/Seamless.h>
#import "MenuCategory.h"
#import "EkrandaAppDelegate.h"

@interface HomeController : CustomViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) MenuCategory* category;
@property (nonatomic, strong) IBOutlet UITableView* mainTable;
@property (nonatomic, strong) NSMutableArray *shownIndexes;
@property (nonatomic, strong) SLTableViewAdManager * adManager;
@property (nonatomic, strong) NSMutableArray * contents;
@property (nonatomic, strong) UIRefreshControl * refreshControl;

@property (nonatomic) BOOL isRefreshed;

-(void)loadUI;
-(void)scrollToTop;
- (BOOL)isAtTop;
-(void) checkUpdate:(EkrandaAppDelegate*)delege;

@end
