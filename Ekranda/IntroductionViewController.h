//
//  IntroductionViewController.h
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroductionViewController : UIViewController <UIPageViewControllerDataSource>


@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageHeaderTexts;
@property (strong, nonatomic) NSArray *pageDetailTexts;
@property (strong, nonatomic) NSArray *pageImages;

@end
