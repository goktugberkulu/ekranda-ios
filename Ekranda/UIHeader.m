//
//  UIHeader.m
//  Ekranda
//
//  Created by Mesut Dağ on 09/12/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "UIHeader.h"
#import "JsonParser.h"

@implementation UIHeader

@synthesize likeImage,likeLabel;

-(void)awakeFromNib{
    [super awakeFromNib];
}

-(IBAction)likeClicked:(id)sender{
    NSLog(@"like clicked for %d",[self contentId]);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        int likeCount = [JsonParser addLike:[self contentId]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [likeImage setImage:[UIImage imageNamed:@"star_ico@2X.png"]];
            [likeLabel setText:[NSString stringWithFormat:@"%d",likeCount]];
            
            NSLog(@"aferim beğendim %d",likeCount);
        });
    });
}

@end
