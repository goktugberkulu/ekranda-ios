//
//  FilterViewController.h
//  EkrandaUI
//
//  Created by Goktug on 02/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) UIImage *bgImage;
@end
