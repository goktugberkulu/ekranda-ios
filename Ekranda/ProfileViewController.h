//
//  ProfileViewController.h
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
