//
//  SaleItemView.m
//  Ekranda
//
//  Created by Mesut Dağ on 27/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "SaleItemView.h"
#import "EkrandaAppDelegate.h"

@implementation SaleItemView


-(void)awakeFromNib{
    [super awakeFromNib];
}

- (IBAction)buttonClicked:(id)sender {
    if(sender == self.btnAddToCard){
        [self.delegate buttonAddToCardClicked:self];
    }
    else if(sender == self.btnSizes){
        [self.delegate buttonSizesClicked:self];
    }
    else if(sender == self.btnWebPage){
        [self.delegate buttonWebPageClicked:self];
    }
}

-(IBAction)shareClicked:(id)sender{
    [self.delegate buttonShareClicked:self];
}

-(IBAction)favoritesClicked:(id)sender{
    [self.delegate buttonAddToFavoritesClicked:self];
}

-(IBAction)zoomClicked:(id)sender{
    [self.delegate buttonZoomClicked:self];
}

-(IBAction)infoClicked:(id)sender{
    
    [self.infoWebView loadHTMLString:[NSString stringWithFormat:@"<html><head><style>BODY { font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 16px; color : #555555;}</style></head><body>%@</body></html>",self.itemInfo] baseURL:nil];
    
    self.infoWebView.opaque = NO;
    
    [UIView transitionWithView:self.containerView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        [self.containerView insertSubview:self.infoView aboveSubview:self.mainView];
    } completion:^(BOOL finished) {
        
    }];
}

-(IBAction)closeClicked:(id)sender{
    [UIView transitionWithView:self.containerView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        [self.containerView insertSubview:self.mainView aboveSubview:self.infoView];
    } completion:^(BOOL finished) {}];
}


+(NSString*)reuseIdentifier {
    return NSStringFromClass([SaleItemView class]);
}

-(void)changeItemSaleStatus:(BOOL)isSale{
    if (!isSale) {
        [self.btnSizes setHidden:YES];
        [self.imageArrow setHidden:YES];
        [self.btnAddToCard setHidden:YES];
        [self.btnInfo setHidden:YES];
       
        [self.labelSpecialPrice setHidden:YES];
        [self.labelInfo setHidden:YES];
        [self.imageInfo setHidden:YES];
        
        if([self.labelPrice.text length]>0){
            [self.labelPrice setHidden:NO];
        }
        else{
            [self.labelBrand setFrame:CGRectMake(self.labelBrand.frame.origin.x, 35, self.labelBrand.frame.size.width, self.labelBrand.frame.size.height)];
            
            [self.labelName setFrame:CGRectMake(self.labelName.frame.origin.x, 56, self.labelName.frame.size.width, self.labelName.frame.size.height)];
            
            [self.labelPrice setHidden:YES];
        }
    }
    else{
        [self.btnSizes setHidden:NO];
        [self.imageArrow setHidden:NO];
        [self.btnAddToCard setHidden:NO];
        [self.btnInfo setHidden:NO];
        [self.labelPrice setHidden:NO];
        [self.labelSpecialPrice setHidden:NO];
        [self.labelInfo setHidden:NO];
        [self.imageInfo setHidden:NO];
        [self.btnWebPage setHidden:YES];
        
        [self.labelBrand setFrame:CGRectMake(self.labelBrand.frame.origin.x, 12, self.labelBrand.frame.size.width, self.labelBrand.frame.size.height)];
        
        [self.labelName setFrame:CGRectMake(self.labelName.frame.origin.x, 33, self.labelName.frame.size.width, self.labelName.frame.size.height)];
    }
}

-(void)disableItemSizes{
    [self.btnSizes setHidden:YES];
    [self.imageArrow setHidden:YES];
}

-(void)disableShare{
    [self.btnShare setHidden:YES];
    [self.shareIco setHidden:YES];
    [self.shareImage setHidden:YES];
    [self.zoomButton setHidden:YES];
}

-(void)disableInfo{
    [self.btnInfo setHidden:YES];
    [self.labelInfo setHidden:YES];
    [self.imageInfo setHidden:YES];
}

-(void)enableItemSizes{
    [self.btnSizes setHidden:NO];
    [self.imageArrow setHidden:NO];
}

-(void)changeSelectedProduct:(NSString*)sizeName{
    [self.btnSizes setTitle:sizeName forState:UIControlStateNormal];
    [self.btnSizes.titleLabel setFont:[UIFont fontWithName:self.btnSizes.titleLabel.font.fontName size:16]];
    self.btnSizes.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.btnSizes.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
}

@end
