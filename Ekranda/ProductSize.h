//
//  ProductSize.h
//  Ekranda
//
//  Created by Mesut Dağ on 17/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductSize : NSObject

@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* size;
@property (nonatomic, strong) NSString* image;
@property (nonatomic) int stock;
@property (nonatomic) int childActive;

@property (nonatomic, strong) NSString* productName;
@property (nonatomic, strong) NSString* productPrice;

@end
