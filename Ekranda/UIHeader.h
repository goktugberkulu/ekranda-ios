//
//  UIHeader.h
//  Ekranda
//
//  Created by Mesut Dağ on 09/12/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIHeader : UIView

@property (nonatomic,strong) IBOutlet UIImageView *mainImage;
@property (nonatomic) int contentId;
@property (nonatomic,strong) IBOutlet UIImageView *likeImage;
@property (nonatomic,strong) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
