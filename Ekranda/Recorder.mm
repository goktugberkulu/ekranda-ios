/**
 * Copyright (c) 2014 Civolution B.V. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Civolution B.V. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Civolution B.V.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : ftristani
 *
 * Recorder.mm
 */

#import "Stream_Reader.h"
#include "Recorder.h"
#import <AVFoundation/AVAudioSession.h>

Recorder::Recorder()
{
	mIsRunning = FALSE;
}

Recorder::~Recorder()
{
	stream_reader = nil;
    AudioQueueDispose(mQueue, TRUE);
}

void Recorder::InitRecorder(Stream_Reader *newStreamReader,Float64 fs, int bitsPerChannel, int bufferSize, int numChannel)
{
	stream_reader = newStreamReader;
    mFs = fs;
    mBitsPerChannel = bitsPerChannel;
    mBufferSize = bufferSize;
    mNumChannel = numChannel;
    try 
    {
        SetupAudio();
	}
	catch (CAXException &e) {
		char buf[256];
		fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
	}
	catch (...) {
		fprintf(stderr, "An unknown error occurred\n");
	}	
}

//Call-back function: called when an audio buffer is available
void Recorder::MyInputBufferHandler(void *								inUserData,
									AudioQueueRef						inAQ,
									AudioQueueBufferRef					inBuffer,
									const AudioTimeStamp *				inStartTime,
									UInt32								inNumPackets,
									const AudioStreamPacketDescription*	inPacketDesc)
{
	Recorder *aqr = (Recorder *)inUserData;
	try 
    {
		if (inNumPackets > 0) 
        {
            //Send audio buffer to dection library
            [aqr->stream_reader readStream:inBuffer->mAudioData];
        }
		// if we're not stopping, re-enqueue the buffer so that it gets filled again
		if (aqr->IsRunning())
        {
			XThrowIfError(AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL), "AudioQueueEnqueueBuffer failed");
        }
	} 
    catch (CAXException e) 
    {
		char buf[256];
		fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
	}
}

void Recorder::StartRecord()
{
	try {
        //Init the AudioQueue with audio record properties and call-back function
		XThrowIfError(AudioQueueNewInput(&mRecordFormat, MyInputBufferHandler, 
                                         this, NULL, NULL, 0, &mQueue), "AudioQueueNewInput failed");
		// get the record format back from the queue's audio converter
		UInt32 size = sizeof(mRecordFormat);
		XThrowIfError(AudioQueueGetProperty(mQueue, kAudioQueueProperty_StreamDescription,	
											&mRecordFormat, &size), "couldn't get queue's format");
        // allocate and enqueue buffers
		for (int i = 0; i < kNumberRecordBuffers; ++i) {
			XThrowIfError(AudioQueueAllocateBuffer(mQueue, mBufferSize, &mBuffers[i]),
						  "AudioQueueAllocateBuffer failed");
			XThrowIfError(AudioQueueEnqueueBuffer(mQueue, mBuffers[i], 0, NULL),
						  "AudioQueueEnqueueBuffer failed");
		}
		// start the queue
		mIsRunning = true;
		XThrowIfError(AudioQueueStart(mQueue, NULL), "AudioQueueStart failed");
	}
	catch (CAXException &e) {
		char buf[256];
		fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
	}
	catch (...) {
		fprintf(stderr, "An unknown error occurred\n");
	}	
}

void Recorder::StopRecord()
{
	mIsRunning=FALSE;
	try
    {   //Stop AudioQueue process
        XThrowIfError(AudioQueueStop(mQueue, true), "AudioQueueStop failed");
    }
	catch (CAXException &e) {
		char buf[256];
		fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
	}
	catch (...) {
		fprintf(stderr, "An unknown error occurred\n");
	}
	//Free AudioQueue allocations
	AudioQueueDispose(mQueue, true);
}

void Recorder::SetupAudio()
{
    //Audio record properties setup
    
    memset(&mRecordFormat, 0, sizeof(mRecordFormat));
    //Setup some record properties according AudioSession properties
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setPreferredSampleRate:mFs error:NULL];
    mRecordFormat.mSampleRate = [audioSession sampleRate];
    mRecordFormat.mChannelsPerFrame = (UInt32)[audioSession inputNumberOfChannels];
    mRecordFormat.mFormatID = kAudioFormatLinearPCM;
    mRecordFormat.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    mRecordFormat.mBitsPerChannel = mBitsPerChannel;
    mRecordFormat.mBytesPerPacket = mRecordFormat.mBytesPerFrame = (mRecordFormat.mBitsPerChannel / 8) * mRecordFormat.mChannelsPerFrame;
    mRecordFormat.mFramesPerPacket = mNumChannel;
}





