//
//  Core.h
//  Ekranda
//
//  Created by Mesut Dağ on 30/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Core : NSObject

@property(nonatomic, readonly, retain) NSUUID *identifierForVendor;

+(void)addLoading:(UIView*)mainView withDim:(BOOL)dim dimString:(NSString*)dimText;
+(void)removeLoading:(UIView*)mainView;
+(NSString *)getDeviceId;
+(void)addToast:(UIView*)mainView withString:(NSString *)toastText;

@end
