//
//  YayinAkisiLine.h
//  TV2
//
//  Created by Mesut Dağ on 22/04/14.
//  Copyright (c) 2014 Turgut EREN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuLine : UITableViewCell

@property(nonatomic, retain) IBOutlet UILabel* titleLabel;

@end
