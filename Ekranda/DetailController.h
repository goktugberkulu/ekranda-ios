//
//  DetailController.h
//  Ekranda
//
//  Created by Mesut Dağ on 23/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import <Seamless/Seamless.h>
#import "Content.h"
#import "SaleItemView.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "MenuCategory.h"

@interface DetailController : CustomViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate,SaleItemViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate>

@property(nonatomic, readonly, retain) NSUUID *identifierForVendor;
@property (nonatomic,strong) Content* content;
@property (nonatomic) int contentDetailId;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic, strong)  UITableView* tableView;


@property (nonatomic, strong)  UIScrollView *itemScroll;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong)  UILabel *detailLabel;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong)  UIView *similarProductsView;
@property (nonatomic, strong)  UIView *commentsView;
@property (nonatomic, strong)  UIView *headerView;
@property (nonatomic, strong) UIButton *writeCommentButton;
@property (nonatomic, strong) UIButton *displayAllCommentsButton;

@property (nonatomic, strong)  UIView *pickerContainer;
@property (nonatomic, strong)  UIView *pickerView;
@property (nonatomic, strong)  UILabel *pickerLabel;

@property (nonatomic, strong) MenuCategory* category;
@property (nonatomic, strong) UIPickerView *pickerSize;
@property (nonatomic, strong) NSMutableArray* sizes;

@property (nonatomic, strong) SLAdView * adView;
@property (nonatomic, strong) SLInterstitialAdManager * intAdManager;
@property (nonatomic, strong) SLTableViewAdManager * adManager;

@property (nonatomic, strong) UIDocumentInteractionController *documentController;

-(IBAction)chooseSizeClicked:(id)sender;


@end
