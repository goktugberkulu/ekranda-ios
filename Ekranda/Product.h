//
//  Product.h
//  Ekranda
//
//  Created by Mesut Dağ on 24/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic) int itemId;
@property (nonatomic, strong) NSString* itemBrand;
@property (nonatomic, strong) NSString* itemName;
@property (nonatomic, strong) NSString* itemImage;
@property (nonatomic, strong) NSString* itemPrice;
@property (nonatomic, strong) NSString* itemStoreLink;
@property (nonatomic) BOOL itemIsSale;
@property (nonatomic) int order;
@property (nonatomic) NSString* itemProductId;
@property (nonatomic) int contentId;
@property (nonatomic, strong) NSString* contentTitle;

@property (nonatomic) NSString* selectedProductId;
@property (nonatomic) NSString* selectedCatalogName;
@property (nonatomic) NSString* itemType;

@end
