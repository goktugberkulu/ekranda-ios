//
//  JsonParser.h
//  Ekranda
//
//  Created by Mesut Dağ on 14/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Content.h"
#import "MenuCategory.h"

//#define isDEBUG TRUE

//#if isDEBUG
//#define s_TAG_GET_CATEGORIES @"http://suscipio.elasticbeanstalk.com/TAGPIA/getCategoryAll"
//#define s_TAG_GET_ARCHIVED_CONTENT @"http://suscipio.elasticbeanstalk.com/TAGPIA/getPastContentsPaginationIphone?pageNum="
//#define s_TAG_GET_ARCHIVED_CONTENT_CATEGORY @"http://suscipio.elasticbeanstalk.com/TAGPIA/getPastContentsByCategoryPaginationIphone?categoryId=@@CATEGORY@@&pageNum="
//#define s_TAG_ADD_LIKE @"http://suscipio.elasticbeanstalk.com/TAGPIA/addLikeIphone?contentId="
//#define s_TAG_GET_CONTENT_DETATIL @"http://suscipio.elasticbeanstalk.com/TAGPIA/getContentDetailsIphone?contentId="
//#define s_TAG_GET_FUTURE_CONTENT @"http://suscipio.elasticbeanstalk.com/TAGPIA/getFutureContentsIphone"

#define s_TAG_GET_CATEGORIES @"http://ekranda.dogannet.tv/api/getCategoryAll"
#define s_TAG_GET_ARCHIVED_CONTENT @"http://ekranda.dogannet.tv/api/getPastContentsPaginationIphone?pageNum="
#define s_TAG_GET_ARCHIVED_CONTENT_CATEGORY @"http://ekranda.dogannet.tv/api/getPastContentsByCategoryPaginationIphone?categoryId=@@CATEGORY@@&pageNum="
#define s_TAG_ADD_LIKE @"http://ekranda.dogannet.tv/api/addLikeIphone?contentId="
#define s_TAG_GET_CONTENT_DETATIL @"http://ekranda.dogannet.tv/api/getContentDetailsIphone?contentId="
#define s_TAG_GET_FUTURE_CONTENT @"http://ekranda.dogannet.tv/api/getFutureContentsIphone"
#define s_FORCE_UPDATE_VERSION @"http://quark.dogannet.tv/mobile/ekranda_ios_version.txt"

//------------------------------------------------------------------------------------------------------------
//#define s_TAG_GET_CATEGORIES @"http://172.16.10.186:8888/api/getCategoryAll"
//#define s_TAG_GET_ARCHIVED_CONTENT @"http://172.16.10.186:8888/api/getPastContentsPaginationIphone?pageNum="
//#define s_TAG_GET_ARCHIVED_CONTENT_CATEGORY @"http://172.16.10.186:8888/api/getPastContentsByCategoryPaginationIphone?categoryId=@@CATEGORY@@&pageNum="
//#define s_TAG_ADD_LIKE @"http://172.16.10.186:8888/api/addLikeIphone?contentId="
//#define s_TAG_GET_CONTENT_DETATIL @"http://172.16.10.186:8888/api/getContentDetailsIphone?contentId="
//#define s_TAG_GET_FUTURE_CONTENT @"http://172.16.10.186:8888/api/getFutureContentsIphone"


//#else

//#define s_TAG_GET_CATEGORIES @"http://suscipio.elasticbeanstalk.com/TAGPIA/getCategoryAll"
//#define s_TAG_GET_ARCHIVED_CONTENT @"http://suscipio.elasticbeanstalk.com/TAGPIA/getPastContentsPaginationIphone?pageNum="
//#define s_TAG_GET_ARCHIVED_CONTENT_CATEGORY @"http://suscipio.elasticbeanstalk.com/TAGPIA/getPastContentsByCategoryPaginationIphone?categoryId=@@CATEGORY@@&pageNum="
//#define s_TAG_ADD_LIKE @"http://suscipio.elasticbeanstalk.com/TAGPIA/addLikeIphone?contentId="
//#define s_TAG_GET_CONTENT_DETATIL @"http://suscipio.elasticbeanstalk.com/TAGPIA/getContentDetailsIphone?contentId="

//#endif



/*

#define s_TAG_GET_CATEGORIES @"http://suscipio.elasticbeanstalk.com/TAGPIA/getCategoryAll"
#define s_TAG_GET_ARCHIVED_CONTENT @"http://suscipio.elasticbeanstalk.com/TAGPIA/getPastContentsPagination?pageNum="

#define s_TAG_GET_CONTENT_DETATIL @"http://suscipio.elasticbeanstalk.com/TAGPIA/getContentDetails?contentId="

#define s_TAG_GET_ARCHIVED_CONTENT_CATEGORY @"http://suscipio.elasticbeanstalk.com/TAGPIA/getPastContentsByCategory?categoryId=@@CATEGORY@@&pageNum="

#define s_TAG_GET_FUTURE_CONTENT @"http://suscipio.elasticbeanstalk.com/TAGPIA/getFutureContents"
#define s_TAG_ADD_LIKE @"http://suscipio.elasticbeanstalk.com/TAGPIA/addLike?contentId="

*/




//#define s_TAG_CHECK_FUTURE_CONTENT @"https://s3.amazonaws.com/tagpia-bucket-01/status"
#define s_TAG_CHECK_FUTURE_CONTENT @"http://ekranda.dogannet.tv/api/status"

#define s_LIDYANA_GET_PRODUCT @"http://lidy.lidyana.com/xml/secondscreen"
#define s_LIDYANA_OPEN_CART @"https://m.lidyana.com/cart?gid=@@GID@@&act=secondscreen"
#define s_LIDYADA_ADD_TO_CART @"https://m.lidyana.com/cart?qadd=@@PRODUCT@@&gid=@@GID@@&act=secondscreen"

#define s_HEPSIBURADA_ADD_TO_CART @"https://www.hepsiburada.com/mc/customer/KdAddProduct?kdmp2b=@@SKU@@|@@CatalogName@@|@@Quantity@@"
#define s_HEPSIBURADA_GET_PRODUCT @"http://213.243.33.176/ekranda/products.ashx"
#define s_HEPSIBURADA_CHECK_CART @"https://www.hepsiburada.com/mc/customer/KdCartItemCount?securetoken=@@TOKEN@@"

@interface JsonParser : NSObject

+(NSMutableArray *)getCategories;
+(NSMutableArray *)getFutureContents;
+(NSMutableArray *)getThirdPartyProducts;
+(NSMutableArray *)getHBProducts;
+(NSMutableArray *)getContents:(MenuCategory *)category withPagaNumber:(int)pageNumber;
+(Content *)getContentDetails:(int)contendID;
+(BOOL)addToCard:(NSString *)productId;
+(NSDictionary *)addToCard:(NSString *)SKU catalogName:(NSString*)cName quantity:(NSString*)quant secureToken:(NSDictionary*)security;
+(BOOL)checkHBCard:(NSDictionary*)security;
+(int)addLike:(int)contentId;
+(BOOL)fcHasChanged;

+(long long) getSecondIncrement;
+(void) setSecondIncrement:(long long)increment;
+(long long) getLastFutureContentTime;

@end
