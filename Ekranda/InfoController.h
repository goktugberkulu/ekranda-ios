//
//  InfoController.h
//  Ekranda
//
//  Created by Mesut Dağ on 18/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"

@interface InfoController : CustomViewController

@property (nonatomic, strong) IBOutlet UIButton* ekrandaNedir;

@end
