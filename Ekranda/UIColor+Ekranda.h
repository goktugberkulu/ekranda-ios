//
//  UIColor+Ekranda.h
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Ekranda)


+ (UIColor *)eiFadedRedColor;
+ (UIColor *)eiWhiteColor;
+ (UIColor *)eiCharcoalGreyColor;
+ (UIColor *)eiWhiteTwoColor;
+ (UIColor *)eiDarkSkyBlueColor;
+ (UIColor *)eiLightishPurpleColor;
+ (UIColor *)eiShadowFadedRedColor;
+ (UIColor *)eiGrayColor;

@end
