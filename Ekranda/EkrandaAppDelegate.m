//
//  AppDelegate.m
//  Ekranda
//
//  Created by Mesut Dağ on 10/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "EkrandaAppDelegate.h"
#import "MenuController.h"
#import "HomeController.h"
#import "SplashView.h"
#import <Seamless/Seamless.h>
#import "JsonParser.h"
#import "DetailController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"


@implementation EkrandaAppDelegate
@synthesize notifView,favorites,hbSecureObject,html5View;
int contentDetailId;

NSTimer *timerFutureContent,*timerProducts;

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    [NetworkClock sharedNetworkClock];
    
    UIViewController* leftSideDrawerViewController = [[MenuController alloc] init];
    HomeController* centerViewController = [[HomeController alloc] init];
    
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
    
    
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navigationController
                             leftDrawerViewController:leftSideDrawerViewController];
    
    [self.drawerController setShowsShadow:YES];
   
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:self.drawerController];
     [self.drawerController setMaximumLeftDrawerWidth: 7 * self.window.frame.size.width / 8];
 
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    if([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]){
        
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        NSDictionary *payload = userInfo[@"payload"];
        
        if([[payload objectForKey:@"action"] isEqualToString:@"detay"]){
            NSString* contentId = [payload objectForKey:@"id"];
            int content = [contentId intValue];
            
            contentDetailId = content;
            
            //[TapTarget ttlog:[NSString stringWithFormat:@"/pushtapped/%@", payload] withValue:@"" andScore:1];
            
        }else{
            [self application:application didReceiveRemoteNotification:[launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
        }
    }

    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [self CreateSplash];
    
    [[SLManager sharedManager] setAppToken:@"fa350c11-3e4d-4594-bb9d-18071284abbe"];
   
    [self setTimers];
    [self ganTrackerInitialization];
    [self getFavorites];
    [self loadHBObject];
    
    
    //[[TapTarget sharedTapTarget] setExtendedLogging:YES];
    
    return YES;
}

-(void)getFavorites{
    
    favorites = [NSMutableArray array];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"Favorites"];
    NSMutableArray* preFav = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(preFav.count)
        favorites = preFav;
}

-(void)saveToFavorites:(Product *)content{
   
    if(![favorites containsObject:content])
        [favorites addObject:content];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:favorites];
    
    [defaults setObject:data forKey:@"Favorites"];
    [defaults synchronize];
    
    MenuController *mc = (MenuController*)self.drawerController.leftDrawerViewController;
    [mc updateFavCount];
    NSLog(@"delegete : favori kaydedildi");
}

-(void)removeFromFavorites:(int)itemId{
    Product* temp = nil;
    for(Product* favP in favorites){
        if(favP.itemId == itemId){
            temp = favP;
            break;
        }
    }

    [favorites removeObject:temp];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:favorites];
    [defaults setObject:data forKey:@"Favorites"];
    [defaults synchronize];
    
    MenuController *mc = (MenuController*)self.drawerController.leftDrawerViewController;
    [mc updateFavCount];
    NSLog(@"delegete : favorilerden silindi");
}

-(void)loadHBObject{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    hbSecureObject = [defaults dictionaryForKey:@"hbtoken"];
    NSLog(@"loaded hbSecureObject");
}

-(void)clearHBObject{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"hbtoken"];
    [defaults synchronize];
    hbSecureObject = nil;
}

-(void)saveHBObject:(NSDictionary *)dictionary{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:dictionary forKey:@"hbtoken"];
    [defaults synchronize];
    hbSecureObject = dictionary;
}

-(void)stopDetector{
    
}

-(void)startDetector{
    
}

-(void)setTimers{
    timerFutureContent = [NSTimer timerWithTimeInterval:180.0
                                    target: self
                                    selector: @selector(updateFutureContents:)
                                  userInfo: nil
                                   repeats: YES];
    
    timerProducts = [NSTimer timerWithTimeInterval:300.0
                                            target: self
                                          selector: @selector(updateFutureContents:)
                                          userInfo: nil
                                           repeats: YES];
    
    [[NSRunLoop mainRunLoop] addTimer:timerFutureContent forMode:NSRunLoopCommonModes];
    [[NSRunLoop mainRunLoop] addTimer:timerProducts forMode:NSRunLoopCommonModes];

}

-(void)updateFutureContents:(NSTimer *)timer{
    NSLog(@"checking future contents");
   
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if([JsonParser fcHasChanged]){
            self.futureContents = [JsonParser getFutureContents];
        }
    });
}

-(void)updateProducts:(NSTimer *)timer{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         self.products = [JsonParser getThirdPartyProducts];
    });
   
}

-(void)ganTrackerInitialization
{
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-56466162-1"];
}

-(void)ganSendView:(NSString *)screenName{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)ganSendAction:(NSString *)category withAction:(NSString *)action withLabel:(NSString *)label{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:nil] build]];
}

-(void)showHome:(MenuCategory *)category withToggle:(BOOL)toggle{
 
    HomeController* centerViewController = [[HomeController alloc] init];
    centerViewController.category = category;
    
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
    
    [self.drawerController setCenterViewController:navigationController];
  
    if(toggle)
        [self.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)addHTMLContent:(NSString *)url{
    
    [html5View removeFromSuperview];
    html5View = nil;
    
    html5View = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height)];
    
    UINavigationController* navController = (UINavigationController*)self.drawerController.centerViewController;
    UIViewController* rvc = [navController.viewControllers lastObject];
    [rvc.view addSubview:html5View];
    [rvc.view bringSubviewToFront:html5View];
    
    UIWebView *uiwv = [[UIWebView alloc] init];
    uiwv.frame = html5View.frame;
    [uiwv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [html5View addSubview:uiwv];
    
    UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(self.window.frame.size.width - 46, 30, 30,30)];
    [btnClose setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(closeHTMLWindow) forControlEvents:UIControlEventTouchUpInside];
    [html5View addSubview:btnClose];
}

-(void)closeHTMLWindow{
    
    [html5View removeFromSuperview];
    html5View = nil;
}

-(void)addNewContentNotification{
    
    [notifView removeFromSuperview];
    notifView = nil;
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"UINotification" owner:self options:nil];
    notifView = [subviewArray objectAtIndex:0];
    
    notifView.layer.shadowOffset = CGSizeMake(0, 10);
    notifView.layer.shadowRadius = 4;
    notifView.layer.shadowOpacity = 0.3;
    
    UIButton* closeButton = (UIButton*)[notifView viewWithTag:1003];
    [closeButton addTarget:self action:@selector(closeNotif) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* mainButton = (UIButton*)[notifView viewWithTag:1004];
    [mainButton addTarget:self action:@selector(notificationAction) forControlEvents:UIControlEventTouchUpInside];
    
    [notifView setFrame:CGRectMake(0, -50, notifView.frame.size.width, notifView.frame.size.height)];
    [notifView setAlpha:0];
    
    UINavigationController* navController = (UINavigationController*)self.drawerController.centerViewController;
    
    UIViewController* rvc = [navController.viewControllers lastObject];
    [rvc.view addSubview:notifView];
    
    
    [UIView animateWithDuration:1
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         notifView.alpha = 0.95;
                         [notifView setFrame:CGRectMake(0, 0, notifView.frame.size.width, notifView.frame.size.height)];
                     }
                     completion:^(BOOL finished) {
                         
    }];
    
}

-(void)closeNotif{
    
    [UIView animateWithDuration:1
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         notifView.alpha = 0;
                         [notifView setFrame:CGRectMake(0, -50, notifView.frame.size.width, notifView.frame.size.height)];
                     }
                     completion:^(BOOL finished) {
                         [notifView removeFromSuperview];
                         notifView = nil;
                     }];
    
}


-(void)notificationAction{
    UINavigationController* navController = (UINavigationController*)self.drawerController.centerViewController;
    
    [self closeNotif];

    UIViewController* rvc = [navController.viewControllers lastObject];
    if([rvc isKindOfClass:[HomeController class]]){
        NSLog(@"neden burdayım ki");
        [rvc performSelector:@selector(scrollToTop) withObject:nil afterDelay:0];
    }
    else
    {
        NSLog(@"bura aslında else olmalı");
        [self showHome:0 withToggle:NO];
    }
}


-(void)CreateSplash
{
    SplashView *splash = [[[NSBundle mainBundle] loadNibNamed:@"SplashView" owner:self options:nil] objectAtIndex:0];
    splash.contentId = contentDetailId;
    [splash setFrame:CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height)];
    [self.window addSubview:splash];
    [self.window bringSubviewToFront:splash];
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //[TapTarget registerPush:deviceToken withEnvironment:kTapTargetPushEnvironmentLive];
    
    NSString* aStr;

    aStr = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    aStr = [aStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *registrationLink = [NSString stringWithFormat:@"%@%@&%@&%@", k_PUSHNOTIFICATION_REGISTRATION, aStr, k_PUSHNOTIFICATION_APP_ID, k_PUSHNOTIFICATION_DEVICE_TYPE];
    NSURL *url = [NSURL URLWithString:registrationLink];
    
    NSLog(@"%@",registrationLink);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"%@", [error description]);
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    //[TapTarget stopSession];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    //[[TapTarget sharedTapTarget] setExtendedLogging:YES];
   
//    if(![TapTarget getCurrentSession])
//        [TapTarget startSession:@"24e9dc9b9b4733d0857199e0125899e6" withBundleId:@"dogantv.ekranda"];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound |UIRemoteNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //read the payload from the incoming push notification
   NSDictionary *payload = userInfo[@"payload"];
   
   if([payload objectForKey:@"action"] != nil &&  [[payload objectForKey:@"action"] isEqualToString:@"detay"]){
    
       UINavigationController* navController = (UINavigationController*)self.drawerController.centerViewController;
       NSString* contentId = [payload objectForKey:@"id"];
       int content = [contentId intValue];
        
        DetailController* dc = [[DetailController alloc] init];
        dc.contentDetailId = content;
        [navController pushViewController:dc animated:YES];
        
    }
//    else{
//        [TapTarget handleNotification:userInfo withNavigationController:(UINavigationController *)self.window.rootViewController];
//   }
//    
//   [TapTarget ttlog:[NSString stringWithFormat:@"/pushtapped/%@", payload] withValue:@"" andScore:1];
}

-(BOOL) needsUpdate {
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSURL* url = [NSURL URLWithString:s_FORCE_UPDATE_VERSION];
    NSData* data = [NSData dataWithContentsOfURL:url];
    if (data) {
//        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
//        if ([lookup[@"resultCount"] integerValue] == 1){
            NSString* appStoreVersion = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            
            NSArray* storeVersionInfo =[appStoreVersion componentsSeparatedByString:@"."];
            NSArray* currentVersionInfo =[currentVersion componentsSeparatedByString:@"."];
            
            NSInteger count = storeVersionInfo.count > currentVersionInfo.count ? storeVersionInfo.count : currentVersionInfo.count;
            
            for (int i = 0; i < count; i++) {
                NSInteger storeNumber = storeVersionInfo.count > i?[[storeVersionInfo objectAtIndex:i] integerValue] : 0;
                NSInteger curNumber = currentVersionInfo.count > i?[[currentVersionInfo objectAtIndex:i] integerValue] : 0;
                if (curNumber > storeNumber) {
                    return NO;
                } else if(curNumber < storeNumber) {
                    return YES;
                }
            }
            return NO;
//        }
    }
    return NO;
}

- (void)applicationWillTerminate:(UIApplication *)application {
   
}

-(void)checkAndOpenMicrophone {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"micOpen"] == nil) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ekranda" message:@"Televizyon sinyalinin algınlaması için mikrofona izin ver." delegate:self cancelButtonTitle:@"İzin Ver" otherButtonTitles:@"Vazgeç", nil];
        [alert show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    MenuController *ltx = (MenuController*)app.drawerController.leftDrawerViewController;
//    if (buttonIndex == 0) {
//        [ltx.connectionButton setTag:0];
//    } else {
//        [ltx.connectionButton setTag:1];
//    }
//    [ltx connectionClicked:ltx.connectionButton];
}
@end
