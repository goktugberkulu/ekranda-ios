//
//  WalkThrough.m
//  Ekranda
//
//  Created by Mesut on 03/02/15.
//  Copyright (c) 2015 Dogan TV Holding. All rights reserved.
//

#import "WalkThrough.h"
#import "EkrandaAppDelegate.h"

@implementation WalkThrough

int totalPage;
int width;

-(void)awakeFromNib{
    
    int i = 0;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    width = screenRect.size.width;
    int height = screenRect.size.height;
    
    totalPage = 4;
    
    for(UIView *childView in [self.mainScroll subviews]){

        [childView setFrame:CGRectMake(i*width, (height < 500 && i!=4) ? self.frame.origin.y-30 : self.frame.origin.y, 320, (height < 500 && i!=4)  ? self.frame.size.height : self.frame.size.height-15)];
        
        i++;
    }
    
   if(height < 500){
       [self.mainText setFrame:CGRectMake(self.mainText.frame.origin.x+12, self.mainText.frame.origin.y, self.mainText.frame.size.width-24, self.mainText.frame.size.height+24)];
       [self.mainText setContentSize:CGSizeMake(self.mainText.contentSize.width, self.mainText.contentSize.height+24)];
    }
    
    [self.mainScroll setContentSize:CGSizeMake((totalPage+1)*width, self.window.frame.size.height)];
}

-(void)removeLast{
    [[[self.mainScroll subviews] objectAtIndex:totalPage] removeFromSuperview];
    totalPage = 3;
    [self.mainScroll setContentSize:CGSizeMake((totalPage+1)*width, self.window.frame.size.height)];
}


-(IBAction)nextClicked:(id)sender{
    
    int page = self.mainScroll.contentOffset.x / self.mainScroll.frame.size.width;
    
    if(page<totalPage){
        [self.mainScroll setContentOffset:CGPointMake(self.mainScroll.frame.size.width*(page+1), 0.0f) animated:YES];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userAgree"];
        [self removeWalkThrough];
        
//        EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
//        [app checkAndOpenMicrophone];
    }
}


-(void)removeWalkThrough
{
    WalkThrough *walkhThroughView = [WalkThrough getWalkThrough];
    if (walkhThroughView)
    {
        [UIView animateWithDuration:.4
                         animations:^{
                             [walkhThroughView setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [walkhThroughView removeFromSuperview];
                         }];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

+(WalkThrough *)getWalkThrough
{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for (UIView *subview in app.window.subviews)
    {
        if ([subview isKindOfClass:[WalkThrough class]])
            return (WalkThrough *)subview;
    }
    return nil;
}

@end
