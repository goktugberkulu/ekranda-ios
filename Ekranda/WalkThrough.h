//
//  WalkThrough.h
//  Ekranda
//
//  Created by Mesut on 03/02/15.
//  Copyright (c) 2015 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalkThrough : UIView

@property (nonatomic, strong) IBOutlet UIScrollView* mainScroll;
@property (nonatomic, strong) IBOutlet UITextView* mainText;
@property (nonatomic) BOOL isSecond;

-(void)removeLast;

@end
