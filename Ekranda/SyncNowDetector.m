//
//  SyncNowDetector.m
//  Ekranda
//
//  Created by Mesut Dağ on 27/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "SyncNowDetector.h"
#include <AudioToolbox/AudioToolbox.h>
#import "SyncClock.h"

@implementation SyncNowDetector

Stream_Reader* streamReader;
SyncClock* syncClock;

@synthesize mTraceText;


- (void) manageInterruption:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    NSNumber* interuptionTypeValue = [interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey];
    NSUInteger interuptionType = [interuptionTypeValue intValue];
    
    if (interuptionType == AVAudioSessionInterruptionTypeBegan)
    {
        NSLog(@"interruption begin");
        [streamReader stopReader];
    }
    else
    {
        NSLog(@"interruption end");
        [streamReader startReader:NO];
    }
}


- (void) audioSessionManagement
{
    NSError *activationError = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(manageInterruption:) name:AVAudioSessionInterruptionNotification object:nil];
    if ([audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&activationError])
    {
        if (![audioSession setActive:YES error:&activationError])
            NSLog(@"%@", [activationError localizedDescription]);
        
    }
    else
        NSLog(@"%@", [activationError localizedDescription]);
    
}

-(void)stop {
//    [streamReader stopReader];
//    streamReader = nil;
    [syncClock stop];
    syncClock = nil;
}


-(void)start {
    
    /*[self audioSessionManagement];
    
    int payloadID;
    int timestamp;
    NSString *license3G;
    
    BOOL timestampLoop = YES;
    BOOL Log = NO;
    
    
    license3G = @"kADdh0jk2BHOMxOMs8PSRIIJ1Tfw69WWCOzsyC5UPZ+1rgB2lYVBiMJZIl3sjw/etEcbyuGZvnEpH8OO5uw4WVaM2l5IZmE+hfssZvLR5jSdn0s2kwnkp4kJVe1TrLesKH+2h6y1TFEq0XQ8bB/+GyMwTiH7TVuJAAtj4FK5w4eMLwCX2AW2wuxYwTuPDmfhmItBAE1Dd0NGQk1oSmEyOUxEdlNEa216Rk1pV1JXbnF3UVpiQWhSOU9vYm0rZFZYTEgvSUdMUEEwZloyY1JCMUxnPT0A";
    
    timestamp = 20;
    payloadID = 16;
    
    streamReader = [[Stream_Reader alloc] initWithUI:self license:license3G payloadID:payloadID timestamp:timestamp timestampLoop:timestampLoop log:Log];
    
    [streamReader performSelector:@selector(startReader:) withObject:[NSNumber numberWithBool:NO] afterDelay:1.0f];*/
    
    syncClock = [[SyncClock alloc] init];
    [syncClock start];
}

- (void) Trace:(NSString *) textToTrace
{
    NSLog(@"%@",textToTrace);
}

-(void) onPayload:(NSNumber *)timeStamp {
    long long crTimeStamp = [timeStamp longLongValue];
    [syncClock setTime:crTimeStamp];
}

@end
