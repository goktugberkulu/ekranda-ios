//
//  CustomSearchBar.h
//  EkrandaUI
//
//  Created by Goktug on 03/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Ekranda.h"

@interface CustomSearchBar : UISearchBar

@property (strong, nonatomic) UIColor *background;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIFont *font;

- (instancetype) initWithBackgroundColor:(UIColor *)background textColor:(UIColor *)textColor font:(UIFont *)font frame:(CGRect) frame;

@end
