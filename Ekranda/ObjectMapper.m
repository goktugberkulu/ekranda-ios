//
//  ObjectMapper.m
//  Ekranda
//
//  Created by Mesut Dağ on 30/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "ObjectMapper.h"
#import "EkrandaAppDelegate.h"


@implementation ObjectMapper

+(Content *)getFutureContentByWatermark:(long long) timeStamp{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"-------------------------------------------");
    for(Content* tc in app.futureContents){
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy HH:mm:ss"];
        
        NSLog(@"server : %lli - %lli   date : %@ - %@", (long long)tc.contentStartDateTime, timeStamp, [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:(long long)tc.contentStartDateTime]], [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeStamp]]);
        
        if((long long)tc.contentStartDateTime == timeStamp) {
            return tc;
        }
    }
    NSLog(@"-------------------------------------------");
    return nil;
}

+(ThirdPartyProduct *)getThirdPartProductById:(NSString *)productId{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];

    BOOL hb = false;
    NSString* prefix = @"hb-";
    
    if([productId hasPrefix:prefix]){
        productId = [productId substringFromIndex:[prefix length]];
        hb = true;
    }
    
    NSMutableArray* tempArray = hb ? app.hbProducts : app.products;
    
    for (ThirdPartyProduct* tpp in tempArray) {
        if([tpp.productId isEqualToString:productId])
            return tpp;
    }
    return nil;
}
@end
