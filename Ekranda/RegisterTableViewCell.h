//
//  RegisterTableViewCell.h
//  EkrandaUI
//
//  Created by Goktug on 01/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UITextField *field;

@end
