//
//  InfoController.m
//  Ekranda
//
//  Created by Mesut Dağ on 18/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "InfoController.h"
#import "WalkThrough.h"
#import "EkrandaAppDelegate.h"

@implementation InfoController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)createWalkThrough:(id)sender {
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Introduction" bundle:[NSBundle mainBundle]];
    UIViewController *walkThrough = [storyboard instantiateInitialViewController];
    [walkThrough setModalPresentationStyle:UIModalPresentationCustom];
    [walkThrough setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    app.window.rootViewController = walkThrough;
    
//    [UIView animateWithDuration:0.3
//                          delay:0.0
//                        options: UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//                         walkThrough.frame = CGRectMake(0, 0, app.window.frame.size.width, app.window.frame.size.height);
//                         walkThrough.alpha = 1;
//                     }
//                     completion:^(BOOL finished){
//                         [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//                         
//                     }];
    
  }


@end
