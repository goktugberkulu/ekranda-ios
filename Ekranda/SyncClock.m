//
//  SyncClock.m
//  Ekranda
//
//  Created by Mesut Dağ on 03/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "SyncClock.h"
#import "EkrandaAppDelegate.h"
#import "ObjectMapper.h"
#import "HomeController.h"
#import "JsonParser.h"

@implementation SyncClock

long long current = 0;
long long seconds;
long long cycleStart = 1136073600;
int nextNotificationDelay = 0;

NSTimer *timer;

-(id)init{

    if(self = [super init]){
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"success" ofType:@"mp3"]] error:nil];
        
    }
    return self;
}

-(void)setTime:(long long) timeToSet{
    if(current == 0){
        
        current = round([[NSDate networkDate] timeIntervalSince1970]);
        while((cycleStart + 99 * 24 * 60 * 60 )< current){
            cycleStart += (99 * 24 * 60 * 60);
        }
    }
    seconds = timeToSet;
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyy hh:mm:ss"];
    NSLog(@"signal detected %@", [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:(cycleStart+seconds)]]);
}

-(long long)getTime{
    return seconds;
}

-(void)start{
    cycleStart = round([[NSDate networkDate] timeIntervalSince1970]);
    
    
    timer = [NSTimer timerWithTimeInterval:1.0
                                             target: self
                                           selector: @selector(onTick:)
                                           userInfo: nil
                                            repeats: YES];
    
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)stop{
    [timer invalidate];
    timer = nil;
    cycleStart = 1136073600;
    seconds = 0;
}

-(void)onTick:(NSTimer *)timer {
    seconds++;
    if (nextNotificationDelay > 0) {
        nextNotificationDelay--;
        if (nextNotificationDelay == 0) {
            cycleStart = round([[NSDate networkDate] timeIntervalSince1970]);
            seconds = 0;
        }
        return;
    }
    
    long long currentDate = cycleStart+seconds;
    Content* mc = [ObjectMapper getFutureContentByWatermark:currentDate];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyy HH:mm:ss"];
    
    NSLog(@"server : %lli   date : %@", currentDate, [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:currentDate]]);
    
    if(mc != nil) {
//        NSLog(@"serkan  %f        server : %lli", mc.contentStartDateTime, seconds + cycleStart);
        nextNotificationDelay = 60;
        [self.player play];
        EkrandaAppDelegate* app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
        if (mc.contentIsHTML5 && [mc.contentHTML5Type isEqualToString:@"full"]) {
            [app addHTMLContent:mc.contentHTML5Url];
        } else {
            
            UINavigationController* navController = (UINavigationController*)app.drawerController.centerViewController;
            
            UIViewController* rvc = [navController.viewControllers lastObject];
            if([rvc isKindOfClass:[HomeController class]]){
                HomeController *home = (HomeController*)rvc;
                BOOL isAtTop = [home isAtTop];
                if(!isAtTop)
                    [app addNewContentNotification];
            } else {
                [app addNewContentNotification];
            }
            
            [self createLocalNotification];
            [app ganSendAction:@"watermark" withAction:@"new" withLabel:mc.contentTitle];
            //[TapTarget ttlog:@"watermark" withValue:@"" andScore:20];
            mc.isNew = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"newContent" object:mc];
        }
    }
}

-(void)createLocalNotification{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
    notification.alertBody = @"Ekranda şuan gördüğün ürünü hemen satın al !";
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

@end
