/**
 * Copyright (c) 2014 Civolution B.V. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Civolution B.V. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Civolution B.V.
 *
 * Platform   : ios
 * Created    : 03/05/2012
 * Author     : ftristani
 *
 * type.h
 */

/*! 
 @header type.h
 @discussion This header regroups all definitions of constants type used by AwmSyncDetector.
 */

/*!
 @enum Mode
 @abstract Enumeration of the two detection modes allowed by the library. 
 These two modes will just have impacts on the value referenceTime of the structure PayloadEvent.
 @constant MODE_FILE In this mode the value referenceTime of the structure PayloadEvent will represent the number of seconds since the first of January 2006.
 @constant MODE_LIVE In this mode the value referenceTime of the structure PayloadEvent will represent the number of samples passed to the library since the beginning of the reading process.
 */
typedef enum 
{
    MODE_FILE =0,
    MODE_LIVE
} Mode;

/*!
 @enum PayloadType
 @abstract Enumeration of payload types (used by delegate method onPayload).
 @constant TYPE_IDENTIFIED Information raised when there will be an content id or a timestamp detected.
 @constant TYPE_NOT_IDENTIFIED Information raised when there will be a not marked content detected.
 @constant TYPE_MARKED_BUT_NOT_IDENTIFIED Information raised when there will be a marked content detected on which we are not able to give information about content id or timestamp.
 */
typedef enum
{
    TYPE_IDENTIFIED=0, 
    TYPE_NOT_IDENTIFIED, 
    TYPE_MARKED_BUT_NOT_IDENTIFIED
} PayloadType;

/*!
 @enum AlarmEventType
 @abstract Enumeration of alarm types of alarms (used by method onAlarm).
 @constant TYPE_INFO This field is just for information.
 @constant TYPE_WARNING This field indicates to the user that something wrong happened but not critical.
 @constant TYPE_ERROR This field indicates a critical error that should be taken into account.
 */
typedef enum 
{
    TYPE_INFO=0,
    TYPE_WARNING,
    TYPE_ERROR
} AlarmEventType;

/*!
 @enum AlarmEventCode
 @abstract Enumeration of alarms codes (used by method onAlarm).
 @constant INFO_NOTHING No error. 
 @constant INFO_CONFIDENCE_VALUE Confidence value associated to the content id or the timestamp (from 0 to 1).
 @constant ERROR_INIT_PROCESS Error during the initialization of the library.                                       
 @constant ERROR_DURING_PROCESS Error during the audio detection process.
 @constant ERROR_SDK_GENERAL General error occurred in the SDK layer.                                      
 @constant ERROR_LICENSE Error during the license verification.                                       
 @constant ERROR_CONFIGURATION_PROCESS Error during the configuration process.
 @constant WARNING_FLUSH_QUEUE The internal audio sample queue has been flushed because current available CPU power was not sufficient to process the audio samples. 
 */
typedef enum 
{
    INFO_NOTHING=0,
    INFO_CONFIDENCE_VALUE,
    ERROR_INIT_PROCESS,
    ERROR_DURING_PROCESS, 
    ERROR_SDK_GENERAL,
    ERROR_LICENSE,
    ERROR_CONFIGURATION_PROCESS,
    WARNING_FLUSH_QUEUE
} AlarmEventCode;

/*!
 @enum SdkDetectorType
 @abstract Enumeration of SetLicense return code.
 @constant DETECTOR_TYPE_ERROR SetLicense() failure: the operation failed due to an invalid license content.
 @constant DETECTOR_TYPE_SYNCNOW SetLicense() succeed: SyncNow detector type.
 @constant DETECTOR_TYPE_SNAP SetLicense() succeed: SNAP detector type.
 */
typedef enum
{
    DETECTOR_TYPE_ERROR,
    DETECTOR_TYPE_SYNCNOW,
    DETECTOR_TYPE_SNAP
} SdkDetectorType;
