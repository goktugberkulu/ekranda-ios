/**
 * Copyright (c) 2014 Civolution B.V. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Civolution B.V. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Civolution B.V.
 *
 * Platform   : ios
 * Created    : 03/05/2012
 * Author     : ftristani
 *
 * AwmSyncDetector.h
 */

/*! 
 @header AwmSyncDetector.h
 @discussion This header regroups all class definitions to use AwmSyncDetector in order to use the ios SDK.
 */

#import <Foundation/Foundation.h>
#import "type.h"

#pragma mark -
#pragma mark PayloadEvent class

/*!
 @class PayloadEvent
 @discussion This class is used to manage parameters raised by delegate method onPayload.
 */
@interface PayloadEvent : NSObject
{
    PayloadType payloadType;
    long long   contentID;
    double      timeStamp; 
    float       confidence; 
    NSInteger   referenceTime;	
    NSInteger   offset;	
}

/*!
 @property payloadType
 @discussion Type of the event (TYPE_IDENTIFIED, TYPE_NOT_IDENTIFIED, TYPE_MARKED_BUT_NOT_IDENTIFIED).
 @see PayloadType PayloadType
 */
@property (nonatomic, assign) PayloadType payloadType;

/*!
 @property contentID
 @discussion  Content id detected during the reading process. Only valid if payloadType == TYPE_IDENTIFIED and if contentID != -1.
 */
@property (nonatomic, assign) long long  contentID;

/*!
 @property timeStamp
 @discussion  Timestamp detected during the reading process. Only valid if payloadType == TYPE_IDENTIFIED and if timeStamp != -1. Expressed in seconds.
 */
@property (nonatomic, assign) double timeStamp;

/*!
 @property confidence
 @discussion  Allows to know the confidence associated to the event raised. Only valid if payloadType == TYPE_IDENTIFIED. It could be from 0 to 1.
 */
@property (nonatomic, assign) float confidence;

/*!
 @property referenceTime
 @discussion Only useful in order to recontruct some detection logs with a start and a stop of a detection.
 @discussion Reference time indicates at what time the event is raised:<br>
 - It represents the number of seconds since the first of January 2006 for MODE_LIVE.<br>
 - It represents the number of milliseconds since the beginning of the reading process in MODE_FILE.<br>
 It should be used with the offset parameters in order to know the exact time since we are detecting the event that just has been raised:  referenceTime - offset.
 
 */
@property (nonatomic, assign) NSInteger referenceTime;

/*!
 @property offset
 @discussion Only useful in order to reconstruct some detection logs with a start and a stop of a detection.<br>
  Offset is expressed in seconds in MODE_LIVE or in milliseconds in MODE_FILE.<br>
  It should be used with the referenceTime parameters in order to know the exact time since we are detecting the event that has just been raised: referenceTime - offset.
 */
@property (nonatomic, assign) NSInteger offset;

@end

#pragma mark -
#pragma mark AlarmEvent class

/*!
 @class AlarmEvent
 @discussion This class is used to manage alarms.
 */
@interface AlarmEvent : NSObject
{
    AlarmEventType type;
    AlarmEventCode code;
    NSString *message;
}

/*!
 @property type
 @discussion Type of alarms (TYPE_INFO, TYPE_WARNING,TYPE_ERROR).
 @see AlarmEventType AlarmEventType
 */
@property (nonatomic, assign) AlarmEventType type;

/*!
 @property code
 @discussion Code gives information about the alarm raised.
 @see AlarmEventCode AlarmEventCode
 */
@property (nonatomic, assign) AlarmEventCode code;

/*!
 @property message
 @discussion Message gives more information than the code itself about the alarm raised.
 */
@property (nonatomic, strong) NSString *message;

@end

#pragma mark -
#pragma mark DetectorParameters class

/*!
 @class DetectorParameters
 @discussion This class is used to initialize parameters needed by the detection library.<br>
 */
@interface DetectorParameters : NSObject
{
    Mode mode; 
    bool timestampLoop;
    int numIdentifierBits;
    int numTimeStampBits;
}

/*!
 @property mode
 @discussion Mode file or live depending on your application. Most of the time it will be mode live.
 @see Mode Mode
 */
@property(nonatomic, assign) Mode mode;

/*!
 @property timestampLoop
 @discussion Option used to inform the detection library if the timestamp will loop when it will reach its maximum value or if it will freeze at the last possible value.<br>
 This mode have to be in concordance with what have been chosen on the embedder side.<br>
 'YES' to loop the timestamp, 'NO' to freeze it.
 */
@property(nonatomic, assign) bool timestampLoop;

/*!
 @property numIdentifierBits
 @discussion Number of bits used for the content id.<br>
 Number of bits for content id must be from 0 to 32 (SyncNow 2G) or from 0 to 36 (SyncNow 3G).<br>
 For Snap detections, it will always be 16 bits for the Id.
 */
@property(nonatomic, assign) int numIdentifierBits;

/*!
 @property numTimeStampBits
 @discussion Number of bits used for the timestamp id.<br>
 Number of bits for the timestamp id must be from be from 0 to 32 (SyncNow 2G) or from 0 to 36 (SyncNow 3G).<br>
 For Snap detections, it will always be 20 bits for the timestamp.
 */
@property(nonatomic, assign) int numTimeStampBits;

@end


/*!
 @class DetectorParameters
 @discussion This class is used to initialize parameters needed by the detection library.<br>
 */
@interface SnapDetectorParameters : NSObject
{
    Mode mode;
    bool timestampLoop;
}

/*!
 @property mode
 @discussion Mode file or live depending on your application. Most of the time it will be mode live.
 @see Mode Mode
 */
@property(nonatomic, assign) Mode mode;

/*!
 @property timestampLoop
 @discussion Option used to inform the detection library if the timestamp will loop when it will reach its maximum value or if it will freeze at the last possible value.<br>
 This mode have to be in concordance with what have been chosen on the embedder side.<br>
 'YES' to loop the timestamp, 'NO' to freeze it.
 */
@property(nonatomic, assign) bool timestampLoop;

@end







#pragma mark -
#pragma mark AwmSyncDetector delegate 
/*!
 @protocol AwmSyncDetectorDelegate
 @discussion This is the definition of AwSynDetectorDelegate that allows retrieving different events: onPayload, onAlarm and onDebug.
 */
@protocol AwmSyncDetectorDelegate <NSObject>

/*!
 @method onPayload
 @abstract Delegate method used to raise detection events.
 @param event Payload event.
 */
- (void) onPayload:(PayloadEvent *) event;

/*!
 @method onAlarm
 @abstract Delegate method used to raise alarm events.
 @param event Alarm event.
 */
- (void) onAlarm:(AlarmEvent *) event;

/*!
 @method onDebug
 @abstract Delegate method used to raise debug information. Implement it to have more information about the behavior of the detection process and initialization.
 @param message Debug message.
 */
- (void) onDebug:(NSString *) message;

@end


#pragma mark -
#pragma mark UtcAbsoluteDateAndTime class

/*!
 @class UtcAbsoluteDateAndTime
 @discussion This class is used for the conversion of the timestamp value into single date and time units (year, month, day, hour, minute and second).
 */
@interface UtcAbsoluteDateAndTime : NSObject
{
    NSInteger year;
    NSInteger month;
    NSInteger day;
    NSInteger hour;
    NSInteger minute;
    NSInteger second;
}

/*!
 @property year
 @discussion Year of UTC date.
 */
@property (nonatomic, assign) NSInteger year;

/*!
 @property month
 @discussion Month of UTC date.
 */
@property (nonatomic, assign) NSInteger month;

/*!
 @property day
 @discussion Day of UTC date.
 */
@property (nonatomic, assign) NSInteger day;

/*!
 @property hour
 @discussion Hour of UTC time.
 */
@property (nonatomic, assign) NSInteger hour;

/*!
 @property minute
 @discussion Minute of UTC time.
 */
@property (nonatomic, assign) NSInteger minute;

/*!
 @property second
 @discussion Second of UTC time.
 */
@property (nonatomic, assign) NSInteger second;

@end


#pragma mark -
#pragma mark AwSynDetector class

/*!
 @class AwmSyncDetector
 @discussion This class is used to instanciate, initiliaze and process audio data.
 @seealso AwmSyncDetectorFactory AwmSyncDetectorFactory
 */
@interface AwmSyncDetector : NSObject
{
    id<AwmSyncDetectorDelegate> __weak delegate;
}

/*!
 @property delegate
 @discussion This is the delegate used to send callback.
 */
@property (nonatomic, weak) id<AwmSyncDetectorDelegate> delegate;

/*!
 @method getVersion:
 @abstract Method used to know the version of the detection library.
 @return Current version of the library as a string.
 */
+ (NSString *) getVersion;

/*!
 @method setLicense:
 @abstract Method used to set the license information given by Civolution support.
 @param license License given from customer support of Civolution.
 @result SdkDetectorType the type of the detection if operation succeed, an error code otherwise.
 */
- (SdkDetectorType) setLicense:(NSString *) license;


/*!
 @method setAudioParameters:numBitsPerchannel:numChannels:buffLength:
 @abstract Method used to set input audio data information.
 @discussion This method must be used with initialize: method.
 @param sampleRate Indicates the sample rate of input audio data.
 @param numBitsPerchannel Indicates the number of bits for one channel of the input audio data (for example if you have PCM 16 bits, it will be 16).
 @param numChannels Indicates the number of channels of input audio data. It will be "2" for stereo, "1" for mono.
 @param buffLength Indicates the length of the buffer passed to the detection library.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) setAudioParameters:(NSInteger) sampleRate numBitsPerchannel:(NSInteger) numBitsPerchannel numChannels:(NSInteger) numChannels buffLength:(NSInteger) buffLength;


/*!
 @method setDetectorParameters:
 @abstract Method used to set specific detector parameters for SyncNow applications.
 @param param Detector parameters.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) setDetectorParameters:(DetectorParameters *) param;

/*!
@method setSnapDetectorParameters:
@abstract Method used to set specific detector parameters for Snap applications.
@param param Detector snap parameters.
@result Boolean value, YES if operation succeed, NO otherwise.
*/
- (BOOL) setSnapDetectorParameters:(SnapDetectorParameters *) param;

/*!
 @method initialize:
 @abstract Method used to init detector.
 @discussion This method must be called after setAudioFormat:numBitsPerchannel:numChannels:numSamples: method.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) initialize;

/*!
 @method setRecorderFilename:
 @abstract Method used to record buffers passed from the detection library.
 @param filename Indicates the complete path and filename where we want to create the recorded file.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) setRecorderFilename:(NSString *) filename;

/*!
 @method setLogFilename:
 @abstract Method used to record logs from the detection library.
 @param filename Indicates the complete path and filename where we want to create the log file.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) setLogFilename:(NSString *) filename;


/*!
 @method pushAudioBuffer:withBuffLength:
 @abstract Method used to send audio buffers to the detection library.
 @discussion Pass the audio samples to the detection library. The samples are then processed and the detection results will be reported through the callbacks defined in the interface.<br>
 Note that for some efficiency reasons, the detector requires that audioSampleBufferLength must be multiple of 2 (if sample is coded on 16 bits) or 4 (if sample is coded on 24 or 32 bits), if an odd value is passed (ex: 1023), the method will return an error code (negative value).<br>
 Pay also attention that this method may block.
 @param pAudioSampleBuffer Pointer to the audio buffer which will be processed.
 @param buffLength Indicates the length of the buffer passed to the detection library.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) pushAudioBuffer:(const char  *) pAudioSampleBuffer withBuffLength:(NSInteger) buffLength;


/*!
 @method getWatermarkPresence:
 @abstract Method used to detect the presence of the watermark.
 @discussion This information is periodically updated every 0.68s, it indicates that the detector is currently estimating that the signal is watermarked.<br>
 For a matter of reliability, it is recommended to call this method at least twice to validate the returned value.<br><br>
 Output latency: as soon as getWatermarkPresence() returns true, getWatermarkPresence() will keep on returning true during at least 3,4 seconds.<br><br>
 Reliability: watermark presence detection false alarm probability is very low, it is highly unlikely that it states that there is a watermark if there is none. <br>
 On the other hand, it may miss presence detection if the signal is too noisy, or resampled.<br>
 Calling GetWatermarkPresence() several times continuously will improve the watermark presence reliability.
 @result Boolean value, YES if a presence of a watermark is detected, NO if it is not watermarked.
 */
- (BOOL) getWatermarkPresence;



/*!
 @method resetHistoricDetection:
 @abstract Method used to reset the detection algorithm parameters.
 @discussion This method may be used when sampling operation is stopped and then resumed.<br> 
 For some reasons (ex: battery consumption) the SDK client may not want to continuously sample the signal, in this use case it is strongly advised to call resetHistoricDetection() before resuming a new sampling.<br>
 The algorithm parameters are then resetted, getting rid of the previous detection context, improving the detection timing on the new sampling.<br>
 Warning: Keep at least 8 seconds between each call, otherwise the detection algorithm will not be able to work properly.
 @result Boolean value, YES if operation succeed, NO otherwise.
 */
- (BOOL) resetHistoricDetection;


/*!
 @method translateIntoAbsoluteDateAndTime:absoluteDateAndTime:
 @abstract Method that converts a timestamp value into an UTC time.
 @discussion This is an helper method used to convert a timestamp  into its corresponding UTC date and time value.<br> 
 The result is provided by filling an UtcAbsoluteDateAndTime object.
 @param timeStampInSeconds the time stamp value in seconds to be converted.
 @param absoluteDateAndTime the time stamp converted into UtcAbsoluteDateAndTime object.
 @result Boolean value, YES if operation succeed, NO otherwise.
 @see UtcAbsoluteDateAndTime UtcAbsoluteDateAndTime
 */
- (BOOL) translateIntoAbsoluteDateAndTime:(double) timeStampInSeconds  absoluteDateAndTime:(UtcAbsoluteDateAndTime **) absoluteDateAndTime;


@end

#pragma mark -
#pragma mark AwSynDetector factory

/*!
 @class AwmSyncDetectorFactory
 @discussion This factory class is used to create/destroy the AwmSyncDetector object.
 @seealso AwmSyncDetector AwmSyncDetector
 */
@interface  AwmSyncDetectorFactory:NSObject
{
}

/*!
 @method createAwmSyncDetector
 @abstract  This static method is used to create the AwmSyncDetector.
 @result A pointer to the object AwmSyncDetector created.
 */ 
+ (AwmSyncDetector *) createAwmSyncDetector;

/*!
 @method destroy:
 @abstract This static method is used to destroy the AwmSyncDetector.
 @param detector AwmSyncDetector object to destroy.
 */
+ (void) destroy:(AwmSyncDetector *) detector;    

@end
