//
//  main.m
//  Ekranda
//
//  Created by Mesut Dağ on 10/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EkrandaAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EkrandaAppDelegate class]));
    }
}
