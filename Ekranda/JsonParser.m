//
//  JsonParser.m
//  Ekranda
//
//  Created by Mesut Dağ on 14/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "JsonParser.h"
#import "ThirdPartyProduct.h"
#import "ProductSize.h"
#import "Product.h"
#import "Core.h"
#import "EkrandaAppDelegate.h"

static long long lastFutureContentTime = 0;
static long long secondIncrement = 0;

@implementation JsonParser

+(NSMutableArray *)getCategories
{
    NSMutableArray* temp = [NSMutableArray array];
    NSMutableArray* categoriesJSON = [self getRootObject:s_TAG_GET_CATEGORIES];
   
    MenuCategory* home = [[MenuCategory alloc] init];
    home.categoryTitle = @"Anasayfa";
    home.categoryType = @"home";
    home.categoryId = 0;
    [temp addObject:home];
    
    
    for(NSDictionary* categoryJSON in categoriesJSON)
    {
       if([[categoryJSON objectForKey:@"categoryIsActive"] boolValue])
       {
           MenuCategory* mc = [[MenuCategory alloc] init];
           mc.categoryTitle = [categoryJSON objectForKey:@"categoryTitle"];
           mc.categoryType = [categoryJSON objectForKey:@"categoryType"];
           mc.categoryUrl = [categoryJSON objectForKey:@"categoryAdvertisement"];
           mc.categoryId = [[categoryJSON objectForKey:@"categoryId"] intValue];
           [temp addObject:mc];
       }
    }
    return temp;
}

+(NSMutableArray *)getContents:(MenuCategory *)category withPagaNumber:(int)pageNumber{
    
    NSMutableArray* temp = [NSMutableArray array];
    
    NSString* link = category ? s_TAG_GET_ARCHIVED_CONTENT_CATEGORY : s_TAG_GET_ARCHIVED_CONTENT;
    
    NSString *url = [NSString stringWithFormat:@"%@%d", link, pageNumber];
    
    //NSString *uri = [NSString stringWithFormat:@"%@", category ? s_TAG_GET_ARCHIVED_CONTENT_CATEGORY : s_TAG_GET_ARCHIVED_CONTENT];
    
    url = [url stringByReplacingOccurrencesOfString:@"@@CATEGORY@@" withString:[NSString stringWithFormat:@"%d",category.categoryId]];
    
    NSMutableArray* contentJSON = [self getRootObject: url];
    
    for(NSDictionary* data in contentJSON) {
        Content* content =[self parseContent:data];
        
        if (content.contentHTML5Type != (id)[NSNull null] && [[content.contentHTML5Type lowercaseString] isEqualToString:@"full"])
            continue;
        
        [temp addObject:content];
    }
    return temp;
}

+(Content *)getContentDetails:(int)contendID{
    NSString* url =[NSString stringWithFormat:@"%@%d",s_TAG_GET_CONTENT_DETATIL,contendID];
    NSDictionary* contentJson = [[self getRootObject:url] objectAtIndex:0];
    
    Content* tc = [self parseContent:contentJson];
    NSMutableArray* items = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pr in [contentJson objectForKey:@"items"])
    {
        Product* tp = [[Product alloc] init];
        tp.itemId = [[pr objectForKey:@"itemId"] intValue];
        tp.itemBrand = [pr objectForKey:@"itemBrand"];
        tp.itemImage = [pr objectForKey:@"itemImage"];
        tp.itemIsSale = [[pr objectForKey:@"itemIsSale"] boolValue];
        tp.itemName = [pr objectForKey:@"itemName"];
        tp.itemPrice = [pr objectForKey:@"itemPrice"];
        tp.itemStoreLink = [pr objectForKey:@"itemStoreLink"];
        tp.order = [[pr objectForKey:@"itemListOrder"] intValue];
        tp.itemProductId = [pr objectForKey:@"itemProductId"];
        tp.contentId = tc.contentId;
        tp.contentTitle = tc.contentTitle;
        
        [items addObject:tp];
    }
    tc.items = items;
    return tc;
}

+(long long) getSecondIncrement { return secondIncrement; }
+(void) setSecondIncrement:(long long)increment{ secondIncrement = increment; }
+(long long) getLastFutureContentTime { return lastFutureContentTime; }

+(NSMutableArray *)getFutureContents {
    
    lastFutureContentTime = [[NSDate networkDate] timeIntervalSince1970];
    secondIncrement = 0;
    
    NSMutableArray* temp = [NSMutableArray array];
    NSMutableArray* contentJSON = [self getRootObject:s_TAG_GET_FUTURE_CONTENT];
    
    for(NSDictionary* content in contentJSON)
            [temp addObject:[self parseContent:content]];
    
    return temp;
}

+(BOOL)fcHasChanged{
    NSDictionary* contentJSON = [self getRootObject:s_TAG_CHECK_FUTURE_CONTENT];
    if (contentJSON.count > 0) {
        long long changeTime = [[contentJSON objectForKey:@"utc"] doubleValue] / 1000;
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy HH:mm:ss"];
        
        NSLog(@"%lli-----------------------------------------------change time %@ %@",changeTime,[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:changeTime]],[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:lastFutureContentTime]]);
        //NSLog(@"last future content time %lld",lastFutureContentTime);
        if(lastFutureContentTime > 0 && changeTime > lastFutureContentTime){
            NSLog(@"future content changed");
            return YES;
        }else
            return NO; // original is no   // for test do yes
    }
    return NO; // original is no // for test do yes
}

+(NSMutableArray *)getThirdPartyProducts{
    NSMutableArray* temp = [NSMutableArray array];
    NSMutableArray* productJson = [self getRootObject:s_LIDYANA_GET_PRODUCT];
    
    for (NSDictionary* cp in productJson){
        
        ThirdPartyProduct* tp = [[ThirdPartyProduct alloc] init];
        tp.productId = [cp objectForKey:@"ID"];
        tp.price = [cp objectForKey:@"PRICE"];
        tp.name = [cp objectForKey:@"NAME"];
        tp.brand = [cp objectForKey:@"BRAND"];
        tp.dizi = @"Beden Seçiniz";
        tp.image = [cp objectForKey:@"IMAGE"];
        tp.url = [cp objectForKey:@"URL"];
        tp.itemDescription = [cp objectForKey:@"DESCRIPTION"];
        tp.specialPrice = [cp objectForKey:@"SPECIALPRICE"];
        tp.isParent = [[cp objectForKey:@"IS_PARENT"] boolValue];
        tp.stock = ([[cp objectForKey:@"STOCK"] respondsToSelector:@selector(intValue)]) ? [[cp objectForKey:@"STOCK"] intValue] : 0;
        tp.parentActive = ([[cp objectForKey:@"PARENT_ACTIVE"] respondsToSelector:@selector(intValue)]) ? [[cp objectForKey:@"PARENT_ACTIVE"] intValue] : 0;
        
        tp.type = @"lidyana";
       
        if(!tp.isParent)
        {
            NSMutableArray* productSizes = [[NSMutableArray alloc] init];
            for(NSDictionary* size in [cp objectForKey:@"SIZES"])
            {
                ProductSize* ps = [[ProductSize alloc] init];
                ps.productId = [size objectForKey:@"PRODUCT_ID"];
                ps.size = [size objectForKey:@"SIZE"];
                ps.stock = [[size objectForKey:@"STOCK"] intValue];
                ps.childActive = ([[size objectForKey:@"CHILD_ACTIVE"] respondsToSelector:@selector(intValue)]) ? [[size objectForKey:@"CHILD_ACTIVE"] intValue] : 0;
                [productSizes addObject:ps];
            }
            tp.items = productSizes;
        }
        else{
            
        }
        
        [temp addObject:tp];
    }
    
    return temp;
}

+(NSMutableArray *)getHBProducts{
    NSMutableArray* temp = [NSMutableArray array];
    NSMutableArray* productJson = [self getRootObject:s_HEPSIBURADA_GET_PRODUCT];
    
    for (NSDictionary* cp in productJson){
        
        ThirdPartyProduct* tp = [[ThirdPartyProduct alloc] init];
        tp.productId = [cp objectForKey:@"ID"];
        tp.price = [cp objectForKey:@"PRICE"];
        tp.name = [cp objectForKey:@"NAME"];
        tp.brand = [cp objectForKey:@"BRAND"];
        tp.catalogName = [cp objectForKey:@"CATALOGNAME"];
        tp.image = [cp objectForKey:@"IMAGE"];
        tp.itemDescription = [cp objectForKey:@"DESCRIPTION"];
        tp.isParent = [[cp objectForKey:@"IS_PARENT"] boolValue];
        tp.stock = ([[cp objectForKey:@"STOCK"] respondsToSelector:@selector(intValue)]) ? [[cp objectForKey:@"STOCK"] intValue] : 0;
        tp.parentActive = ([[cp objectForKey:@"PARENT_ACTIVE"] respondsToSelector:@selector(intValue)]) ? [[cp objectForKey:@"PARENT_ACTIVE"] intValue] : 0;
        
        tp.type = @"hepsiburada";
        
        if(!tp.isParent)
        {
            NSMutableArray* productSizes = [[NSMutableArray alloc] init];
            NSDictionary* property = [cp objectForKey:@"PROPERTY"];
            tp.dizi = [property objectForKey:@"NAME"];
            for(NSDictionary* size in [property objectForKey:@"SIZES"])
            {
                ProductSize* ps = [[ProductSize alloc] init];
                ps.productId = [size objectForKey:@"PRODUCT_ID"];
                ps.size = [size objectForKey:@"NAME"];
                ps.image = [size objectForKey:@"IMAGE"];
                ps.productName = [size objectForKey:@"PRODUCT_NAME"];
                ps.productPrice = [size objectForKey:@"PRICE"];
                ps.stock = [[size objectForKey:@"STOCK"] intValue];
                ps.childActive = ([[size objectForKey:@"CHILD_ACTIVE"] respondsToSelector:@selector(intValue)]) ? [[size objectForKey:@"CHILD_ACTIVE"] intValue] : 0;
                [productSizes addObject:ps];
            }
            tp.items = productSizes;
        }
        else{
            
        }
        
        [temp addObject:tp];
    }

    
    return temp;
}

+(BOOL)checkHBCard:(NSDictionary*)security{
    NSString* url = [NSString stringWithFormat:@"%@",s_HEPSIBURADA_CHECK_CART];
    url = [url stringByReplacingOccurrencesOfString:@"@@TOKEN@@" withString:[security objectForKey:@"token"]];
    
    
    @try{
        NSDictionary *secureDict = [self getRootObject:url];
        if([[secureDict objectForKey:@"cartItemCount"] intValue] > 0)
            return YES;
    }
    @catch (NSException *exception){
        NSLog(@"exception when adding to card %@",exception.reason);
        return NO;
    }

    return NO;
}

+(BOOL)addToCard:(NSString *)productId{
    NSString* deviceId = [Core getDeviceId];
    NSString* url = [NSString stringWithFormat:@"%@",s_LIDYADA_ADD_TO_CART];
    url = [[url stringByReplacingOccurrencesOfString:@"@@PRODUCT@@" withString:productId] stringByReplacingOccurrencesOfString:@"@@GID@@" withString:deviceId];
    
    @try{
        [self getRootObject:url];
        return YES;
    }
    @catch (NSException *exception){
        NSLog(@"exception when adding to card %@",exception.reason);
        return NO;
    }
}

+(NSDictionary *)addToCard:(NSString *)SKU catalogName:(NSString*)cName quantity:(NSString*)quant secureToken:(NSDictionary*)security{
   
    NSString* url = [NSString stringWithFormat:@"%@",s_HEPSIBURADA_ADD_TO_CART];
    url = [[[url stringByReplacingOccurrencesOfString:@"@@SKU@@" withString:SKU] stringByReplacingOccurrencesOfString:@"@@CatalogName@@" withString:cName] stringByReplacingOccurrencesOfString:@"@@Quantity@@" withString:quant];
    
    if(security!=nil && [[security objectForKey:@"token"] length] != 0){
        url = [url stringByAppendingString:[NSString stringWithFormat:@"&SecureToken=%@",[security objectForKey:@"token"]]];
    }
    
    @try{
        NSDictionary *secureDict = [self getRootObject:url];
        return secureDict;
    }
    @catch (NSException *exception){
        NSLog(@"exception when adding to card %@",exception.reason);
        return nil;
    }
    return nil;
}

+(int)addLike:(int)contentId{
    NSString* url = [NSString stringWithFormat:@"%@%d",s_TAG_ADD_LIKE,contentId];
    NSDictionary* root = [[self getRootObject:url] objectAtIndex:0];
    
    return [[root objectForKey:@"contentLikeCount"] intValue];
}


+(Content *)parseContent:(NSDictionary* ) dictionary{
    Content* tc = [[Content alloc] init];
    tc.contentId = [[dictionary objectForKey:@"contentId"] intValue];
    tc.categoryId = [[dictionary objectForKey:@"categoryId"] intValue];
    tc.contentImgUrl = [dictionary objectForKey:@"contentImageUrl"];
    tc.contentTitle = [dictionary objectForKey:@"contentTitle"];
    tc.contentLikeCount = [[dictionary objectForKey:@"contentLikeCount"] intValue];
    tc.contentStartDateTime = [[dictionary objectForKey:@"contentStartDateTime"] doubleValue] / 1000;
    tc.contentEndDateTime = [[dictionary objectForKey:@"contentEndDateTime"] doubleValue] / 1000;
    tc.contentHTML5Type = [dictionary objectForKey:@"contentHTML5Type"];
    tc.contentHTML5Url = [dictionary objectForKey:@"contentHTML5Url"];
    tc.contentIsHTML5 = [[dictionary objectForKey:@"contentIsHTML5"] boolValue];
    [tc setObjectStatus];
    return tc;
}



+(id)getRootObject:(NSString*) url
{
    NSLog(@"jFor : %@",url);
    url = [url stringByAddingPercentEscapesUsingEncoding:
           NSUTF8StringEncoding];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                             cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:60.0];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    if(response == nil)
        return [[NSMutableArray alloc] init];
    
    return [NSJSONSerialization JSONObjectWithData:response options:0 error:nil];
}

@end
