/**
 * Copyright (c) 2014 Civolution B.V. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Civolution B.V. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Civolution B.V.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : ftristani
 *
 * Stream_Reader.mm
 */

#import "Stream_Reader.h"
#import "SyncNowDetector.h"
#import "Recorder.h"

#define BUFF_SIZE      8192
#define FS              24000
#define BITSPERCHANNEL  16
#define NUMCHANNEL      1

#pragma mark -
#pragma mark Stream_Reader class

@implementation Stream_Reader

Recorder* recorder;

@synthesize mIsRunning;
@synthesize mIsRecording;

- (id) initWithUI:(SyncNowDetector *) VC license:(NSString *) license payloadID:(int) payloadID timestamp:(int) timestamp timestampLoop:(BOOL) timestampLoop log:(BOOL) log
{
    if (self = [super init])
    {
        mUI = VC;
        recorder = new Recorder();
        recorder->InitRecorder(self, FS, BITSPERCHANNEL, BUFF_SIZE, NUMCHANNEL);
        self.mIsRunning = NO;
        self.mIsRecording = NO;
        mTimestamp = timestamp;
        mPayloadID = payloadID;
        mLicense = license;
        mTimestampLoop = timestampLoop;
        mLog = log;
    }
    return self;
}

-(NSString *) getFilePath:(NSString *) name withExtension:(NSString *) ext
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    int inc = 1;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@%d%@", documentsDirectory, name, inc, ext];
    while([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        inc++;
        filePath = [NSString stringWithFormat:@"%@/%@%d%@",
                    documentsDirectory, name, inc, ext];
        
    }
    return filePath;
}

-(NSString *) getFileName:(NSString *) path
{
    NSArray *pathComponents = [path pathComponents];
    return [pathComponents objectAtIndex:([pathComponents count] - 1)];
}

- (void)dealloc
{
    if (self.mIsRunning)
        recorder->StopRecord();
    delete recorder;
    [AwmSyncDetectorFactory destroy:mDetector];
}

- (NSString *) getVersion
{
    return [AwmSyncDetector getVersion]; 
}

- (BOOL) getWmkPresence
{
    return [mDetector getWatermarkPresence];
}

- (void) startReader:(NSNumber *) record
{
    // AwSyncDetector creation via factory
    if ((mDetector = [AwmSyncDetectorFactory createAwmSyncDetector]) != nil)
    {
        SdkDetectorType detectorType = DETECTOR_TYPE_ERROR;
        NSLog(@"***********************\n* Start Stream Reader *\n***********************\n\n\n");
        
        //Set delegate just after AwSyncDetector creation
        mDetector.delegate = self;
        
        //Set license
        detectorType = [mDetector setLicense:mLicense];
        if ( DETECTOR_TYPE_ERROR == detectorType)
        {
            [mUI performSelectorOnMainThread: @selector(Trace:) withObject:@"Error during license setting\n"  waitUntilDone:NO];
            NSLog(@"Error during license setting\n");
            return;
        }
        //Activate recording feature
        if ([record boolValue])
        {
            NSString *recordFilePath = [self getFilePath:@"SyncNow_" withExtension:@".wav"];
            NSString *recordFileName = [self getFileName:recordFilePath];
            if (![mDetector setRecorderFilename:recordFilePath])
            {
                NSLog(@"Error during record option activation\n");
                return;
            }
            [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"Record: %@\n", recordFileName]  waitUntilDone:NO];
        }
        //Activate log feature
        if (mLog)
        {
            NSString *logFilePath = [self getFilePath:@"SyncNow_" withExtension:@".txt"];
            NSString *logFileName = [self getFileName:logFilePath];
            if (![mDetector setLogFilename:logFilePath])
            {
                NSLog(@"Error during log option activation\n");
                return;
            }
            [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"Log: %@\n", logFileName]  waitUntilDone:NO];
        }
        //Set audio parameters
        if ( ![mDetector setAudioParameters:FS numBitsPerchannel:BITSPERCHANNEL numChannels:NUMCHANNEL buffLength:BUFF_SIZE])
        {
            
            NSLog(@"Error during audio format parameters initialization\n");
            return;
        }
        //Set detector parameters
        if (DETECTOR_TYPE_SYNCNOW == detectorType)
        {
            DetectorParameters *param = [[DetectorParameters alloc] init];
            param.mode = MODE_LIVE;
            param.numIdentifierBits = mPayloadID;
            param.numTimeStampBits = mTimestamp;
            param.timestampLoop = mTimestampLoop;
            if (![mDetector setDetectorParameters:param])
            {
                NSLog(@"Error during syncnow detector parameters initialization\n");
                return;
            }
        }
        if (DETECTOR_TYPE_SNAP == detectorType)
        {
            SnapDetectorParameters *param = [[SnapDetectorParameters alloc] init];
            param.mode = MODE_LIVE;
            param.timestampLoop = mTimestampLoop;
            if (![mDetector setSnapDetectorParameters:param])
            {
                NSLog(@"Error during snap detector parameters initialization\n");
                return;
            }
        }
        //Init detector with all parameters and start detection
        if (![mDetector initialize])
            NSLog(@"Error in detector initialization\n");
        else
            recorder->StartRecord();
    }
    else
    {
        [mUI performSelectorOnMainThread: @selector(Trace:) withObject:@"Detector not initialized, armv6 not supported\n"  waitUntilDone:NO];
    }
    self.mIsRunning=YES;
}

- (void) stopReader
{
    self.mIsRunning=NO;
    if (mDetector != nil)
    {
        NSLog(@"*********************\n* End Stream Reader *\n*********************\n\n\n");
        recorder->StopRecord();
        [AwmSyncDetectorFactory destroy:mDetector];
        mDetector = nil;
        [mFileHandler closeFile];
    }
}

- (int) readStream:(void *) buffer
{
    if (mDetector)
        if(![mDetector pushAudioBuffer:(const char *) buffer withBuffLength:BUFF_SIZE])
           NSLog(@"Error in pushAudioBuffer");
    return 0;
}

#pragma mark -
#pragma mark AwmSyncDetector delegate methods

- (void) onPayload:(PayloadEvent *) event
{
    @autoreleasepool
    {
        NSString *header = @"[OnPayload] ";
        if (event.payloadType == TYPE_IDENTIFIED)
        {
            if ((event.contentID != -1) && (event.timeStamp == -1))
            {
                [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"%@ StaticID detected: %lld\t\tConfidence: %f\n", header,event.contentID, event.confidence]  waitUntilDone:NO];
            }
            if ((event.timeStamp != -1) && (event.contentID == -1))
            {
                UtcAbsoluteDateAndTime *utcAbsoluteDateAndTime = [[UtcAbsoluteDateAndTime alloc] init];
                [mDetector translateIntoAbsoluteDateAndTime:event.timeStamp absoluteDateAndTime:&utcAbsoluteDateAndTime];
                NSString *s_utcAbsoluteDateAndTime = [NSString stringWithFormat:@"UTC %02d-%02d-%02d %02d:%02d:%02d", (int)utcAbsoluteDateAndTime.year,  (int)utcAbsoluteDateAndTime.month,  (int)utcAbsoluteDateAndTime.day,  (int)utcAbsoluteDateAndTime.hour,  (int)utcAbsoluteDateAndTime.minute,  (int)utcAbsoluteDateAndTime.second];
                [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"%@ Timestamp detected: %f (%@)\t\tConfidence: %f\n", header, event.timeStamp, s_utcAbsoluteDateAndTime, event.confidence]  waitUntilDone:NO];
                
                [mUI performSelectorOnMainThread:@selector(onPayload:) withObject:[NSNumber numberWithDouble:event.timeStamp] waitUntilDone:NO];
            }
        }
        else if (event.payloadType == TYPE_NOT_IDENTIFIED)
        {
            [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"%@ Content not marked\n", header]  waitUntilDone:NO];
        }
        else if (event.payloadType == TYPE_MARKED_BUT_NOT_IDENTIFIED)
        {
            [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"%@ Content marked but not identified\n", header]  waitUntilDone:NO];
        }
    }   
}

- (void) onAlarm:(AlarmEvent *) event
{
    @autoreleasepool
    {
        NSString *type;
        switch (event.type ) {
            case TYPE_INFO:
            default:
                type = @"INFO";
                break;
            case TYPE_ERROR:
                type = @"ERROR";
                break;
            case TYPE_WARNING:
                type = @"WARNING";
                break;
        }
        NSString *header = [NSString stringWithFormat:@"[OnAlarm]: %@", type];
        if ( INFO_CONFIDENCE_VALUE != event.code )
        {
            [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"%@ %@\n", header, event.message]  waitUntilDone:NO];
        }
    }
}

- (void) onDebug:(NSString *) message
{
    @autoreleasepool
    {
        NSString *header = @"[OnDebug] ";
        [mUI performSelectorOnMainThread: @selector(Trace:) withObject:[NSString stringWithFormat:@"%@ %@\n", header, message]  waitUntilDone:NO];
    }
}

@end
