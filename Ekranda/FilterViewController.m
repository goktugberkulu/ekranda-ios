//
//  FilterViewController.m
//  EkrandaUI
//
//  Created by Goktug on 02/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FilterViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Ekranda.h"

@interface FilterViewController ()
@property (weak, nonatomic) IBOutlet UIButton *castButton;
@property (weak, nonatomic) IBOutlet UIButton *seriesButton;
@property (weak, nonatomic) IBOutlet UIButton *okButtonBig;
@property (weak, nonatomic) IBOutlet UILabel *filterLabel;
@property (weak, nonatomic) IBOutlet UIButton *womanButton;
@property (weak, nonatomic) IBOutlet UIButton *manButton;
@property (weak, nonatomic) IBOutlet UIButton *childButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *accessoryButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *okButtonSmall;
@property (weak, nonatomic) IBOutlet UILabel *pickerTypeLabel;
@property (weak, nonatomic) IBOutlet UIView *pickerHeaderView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) NSArray *castPickerData;
@property (strong, nonatomic) NSArray *seriesPickerData;
@property (strong, nonatomic) NSString *latestPick;
@property (nonatomic) BOOL isPickerSenderFromCast;
@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottomView.hidden = YES;
    [self setBorderFor:self.manButton];
    [self setBorderFor:self.womanButton];
    [self setBorderFor:self.childButton];
    [self setBorderFor:self.accessoryButton];
    [self setBorderFor:self.castButton];
    [self setBorderFor:self.seriesButton];
    self.backgroundImageView.image = self.bgImage;
    self.backgroundImageView.alpha = 0.15;
    self.view.opaque = NO;
    self.backgroundImageView.opaque = NO;
    self.castPickerData = @[@"İlker Kaleli", @"Musa Uzunlar", @"Ataberk Mutlu", @"Burçin Terzioğlu", @"Ece Özdikici", @"İsmail Düvenci", @"Ali İl", @"Kıvanç Tatlıtuğ", @"Çağatay Ulusoy", @"Serenay Sarıkaya"];
    self.seriesPickerData = @[@"Poyraz Karayel", @"Dizi1", @"Dizi2", @"Dizi3", @"Dizi4", @"Dizi5", @"Dizi6", @"Dizi7", @"Dizi8", @"Dizi9"];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setBorderFor:(UIButton *)button {
    button.layer.borderWidth = 1.0f;
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    button.layer.cornerRadius = 16.0f;
}

- (IBAction)pickCompleted:(UIButton *)sender {
    self.bottomView.hidden = YES;
    self.castButton.hidden = NO;
    self.okButtonBig.hidden = NO;
    self.seriesButton.hidden = NO;
    if (self.isPickerSenderFromCast) {
       self.castButton.titleLabel.text = self.latestPick;
    } else {
        self.seriesButton.titleLabel.text = self.latestPick;
    }
    
}

- (IBAction)closePage:(UIButton *)sender {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)filterCategory:(UIButton *)sender {
    if (sender.backgroundColor == nil) {
        sender.layer.borderColor = [UIColor eiFadedRedColor].CGColor;
        sender.backgroundColor = [UIColor eiFadedRedColor];
    } else {
        sender.layer.borderColor = [UIColor whiteColor].CGColor;
        sender.backgroundColor = nil;
    }
}

- (IBAction)filterPicker:(UIButton *)sender {
    if (sender == self.castButton) {
        self.pickerTypeLabel.text = @"Oyuncuya Göre :";
        self.isPickerSenderFromCast = YES;
    } else if (sender == self.seriesButton) {
        self.pickerTypeLabel.text = @"Diziye Göre :";
        self.isPickerSenderFromCast = NO;
    }
    [self.pickerView reloadAllComponents];
    self.bottomView.hidden = NO;
    self.castButton.hidden = YES;
    self.okButtonBig.hidden = YES;
    self.seriesButton.hidden = YES;
}

- (IBAction)filterCompleted:(UIButton *)sender {
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (self.isPickerSenderFromCast) {
        return self.castPickerData.count;
    }
    return self.seriesPickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (self.isPickerSenderFromCast) {
        return [self.castPickerData objectAtIndex:row];
    } else {
        return [self.seriesPickerData objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
   // self.castButton.titleLabel.text = [[self.pickerData objectAtIndex:row] uppercaseString];

    if (self.isPickerSenderFromCast) {
        self.latestPick = [[self.castPickerData objectAtIndex:row] uppercaseString];
        self.castButton.layer.borderColor = [UIColor eiFadedRedColor].CGColor;
        self.castButton.backgroundColor = [UIColor eiFadedRedColor];
    } else {
        self.latestPick = [[self.seriesPickerData objectAtIndex:row] uppercaseString];
        self.seriesButton.layer.borderColor = [UIColor eiFadedRedColor].CGColor;
        self.seriesButton.backgroundColor = [UIColor eiFadedRedColor];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
