//
//  UIFont+Ekranda.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "UIFont+Ekranda.h"

@implementation UIFont (Ekranda)

+ (UIFont *)eiHeaderTitleSFFont {
    return [UIFont systemFontOfSize:18.0 weight: UIFontWeightRegular];
}

+ (UIFont *)eiNavigationSearchPlaceholderFont {
    return [UIFont systemFontOfSize:14.0 weight: UIFontWeightRegular];
}

+ (UIFont *)eiCellLabelFont {
    return [UIFont systemFontOfSize:8.5 weight: UIFontWeightRegular];
}

+ (UIFont *)eiCellTitleFont {
    return [UIFont systemFontOfSize:6.5 weight: UIFontWeightRegular];
}

@end
