//
//  CustomSearchController.m
//  EkrandaUI
//
//  Created by Goktug on 03/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "CustomSearchController.h"
#import "UIColor+Ekranda.h"

@interface CustomSearchController ()

@end

@implementation CustomSearchController


- (instancetype)initWithViewController:(UIViewController *)searchResultViewController backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor font:(UIFont *)font frame:(CGRect) frame {
    if (self = [super init]) {
        self.customSearchBar = [[CustomSearchBar alloc] initWithBackgroundColor:backgroundColor textColor:textColor font:font frame:frame];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
