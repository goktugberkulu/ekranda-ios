//
//  SimilarProductsCollectionViewController.h
//  EkrandaUI
//
//  Created by Goktug on 15/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimilarProductsCollectionViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@end
