//
//  LoginViewController.h
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EkrandaAppDelegate.h"

@interface LoginViewController : UIViewController <UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)userEnter:(UIButton *)sender;
- (IBAction)connectWithTwitter:(UIButton *)sender;
- (IBAction)connectWithFacebook:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
