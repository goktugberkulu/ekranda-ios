//
//  IntroductionContentViewController.m
//  EkrandaUI
//
//  Created by Goktug on 28/07/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "IntroductionContentViewController.h"

#import "UIFont+Ekranda.h"
#import "UIColor+Ekranda.h"

@interface IntroductionContentViewController ()

@end

@implementation IntroductionContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor eiWhiteColor];
    self.topImageView.image = [UIImage imageNamed:self.imageFile];
    self.headerLabel.text = self.headerText;
    self.detailLabel.text = self.detailText;
    
    if (self.pageIndex == 3) {
        self.skipButton.alpha = 1.0;
    } else {
        self.skipButton.alpha = 0.0;
    }
    
    self.headerLabel.font = [UIFont systemFontOfSize:20.0 weight:UIFontWeightBold];
    self.detailLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightLight];
    self.indicator.currentPage = self.pageIndex;
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (IBAction)skipPage:(UIButton *)sender {
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"User" bundle:[NSBundle mainBundle]];
    UIViewController* viewController = [storyboard instantiateInitialViewController];
    [viewController setModalPresentationStyle:UIModalPresentationCustom];
    [viewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:viewController animated:YES completion:nil];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
