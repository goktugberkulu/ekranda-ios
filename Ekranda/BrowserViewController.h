//
//  BrowserViewController.h
//  Ekranda
//
//  Created by Mesut Dağ on 02/11/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"

#define s_GO_LIDYANA 1
#define S_GO_HEPSIBURADA 2

@interface BrowserViewController : CustomViewController <UIWebViewDelegate>

@property(nonatomic, strong) IBOutlet UIWebView* eWebView;
@property (nonatomic) BOOL isWebPage;
@property (nonatomic) int goType;
@property (nonatomic) BOOL forceLoadUrl;

-(void)loadUrl;
-(void)loadProductUrl:(NSString *)url;


@end
