//
//  SplashController.m
//  Ekranda
//
//  Created by Mesut Dağ on 10/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import "SplashView.h"
#import "EkrandaAppDelegate.h"
#import "JsonParser.h"
#import "HomeController.h"
#import "MenuController.h"
#import "Reachability.h"
#import "WalkThrough.h"
#import "DetailController.h"

@implementation SplashView

-(void)awakeFromNib{
    
    [self performSelector:@selector(startBackgroundOperations) withObject:nil afterDelay:.2];
    if ([[UIScreen mainScreen] bounds].size.height < 560) {
        [self.imageView setImage:[UIImage imageNamed:@"logo.png"]];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)removeSplash
{
    SplashView *splash = [SplashView getSplash];
    if (splash)
    {
        [UIView animateWithDuration:.3
                         animations:^{
                             [splash setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [splash removeFromSuperview];
                         }];
    }
    
}

+(SplashView *)getSplash
{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for (UIView *subview in app.window.subviews)
    {
        if ([subview isKindOfClass:[SplashView class]])
            return (SplashView *)subview;
    }
    return nil;
}

#pragma mark - BACKGROUND OPERATIONS

-(void)startBackgroundOperations
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bağlantı hatası"
                                                        message:@"Bir hata oluştu. Lütfen internet bağlantınızı kontrol edin."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
        [self performSelectorInBackground:@selector(loadAllCategories) withObject:nil];
}

-(void)loadAllCategories
{
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    app.categories = [JsonParser getCategories];
    app.futureContents = [JsonParser getFutureContents];
    app.products = [JsonParser getThirdPartyProducts];
    app.hbProducts = [JsonParser getHBProducts];
    [self performSelectorOnMainThread:@selector(allOperationsAreDone) withObject:nil waitUntilDone:YES];
}

-(void)allOperationsAreDone
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"userAgree"])
        [self CreateWalkThrough];
   
    [self removeSplash];
   
    EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
    MenuController *ltx = (MenuController*)app.drawerController.leftDrawerViewController;
    [ltx loadUI:app.categories];
    
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"micOpen"] != nil) {
//        if([[NSUserDefaults standardUserDefaults] boolForKey:@"micOpen"])
            [ltx startDetectorWithImages];
//    }
    
    if(self.contentId){
        UINavigationController* navController = (UINavigationController*)app.drawerController.centerViewController;
        
        DetailController* dc = [[DetailController alloc] init];
        dc.contentDetailId = self.contentId;
        [navController pushViewController:dc animated:YES];
    }
}



-(void)CreateWalkThrough {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isFirstLogin = [defaults boolForKey:@"firstLogin"];
    if (!isFirstLogin) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Introduction" bundle:[NSBundle mainBundle]];
        UIViewController *walkThrough = [storyboard instantiateInitialViewController];
        [walkThrough setModalPresentationStyle:UIModalPresentationCustom];
        [walkThrough setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        EkrandaAppDelegate *app = (EkrandaAppDelegate *)[[UIApplication sharedApplication] delegate];
            app.window.rootViewController = walkThrough;
    }
    
    [defaults setBool:YES forKey:@"firstLogin"];

}

@end
