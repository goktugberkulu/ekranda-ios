//
//  SplashController.h
//  Ekranda
//
//  Created by Mesut Dağ on 10/10/14.
//  Copyright (c) 2014 Dogan TV Holding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashView : UIView

@property (nonatomic, strong) IBOutlet UIImageView* imageView;
@property (nonatomic) int contentId;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *test;

@end
