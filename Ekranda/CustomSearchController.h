//
//  CustomSearchController.h
//  EkrandaUI
//
//  Created by Goktug on 03/08/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSearchBar.h"

@interface CustomSearchController : UISearchController

@property (strong, nonatomic) CustomSearchBar *customSearchBar;

- (instancetype)initWithViewController:(UIViewController *)searchResultViewController backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor font:(UIFont *)font frame:(CGRect) frame;

@end
